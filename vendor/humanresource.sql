-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 30, 2022 at 03:17 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `humanresource`
--

-- --------------------------------------------------------

--
-- Table structure for table `absenkeluar`
--

CREATE TABLE `absenkeluar` (
  `id` int(11) NOT NULL,
  `absen_id` varchar(20) NOT NULL,
  `tanggal` varchar(20) NOT NULL,
  `waktu` time NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `absenkeluar`
--

INSERT INTO `absenkeluar` (`id`, `absen_id`, `tanggal`, `waktu`) VALUES
(1, 'ABS00000001', '30-08-2022', '20:10:20'),
(2, 'ABS00000002', '30-08-2022', '20:11:23'),
(3, 'ABS00000003', '30-08-2022', '20:12:50');

-- --------------------------------------------------------

--
-- Table structure for table `absensi`
--

CREATE TABLE `absensi` (
  `id` varchar(20) NOT NULL,
  `karyawanID` varchar(20) NOT NULL,
  `absensi` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL,
  `date` varchar(20) NOT NULL,
  `time` time NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `absensi`
--

INSERT INTO `absensi` (`id`, `karyawanID`, `absensi`, `status`, `date`, `time`) VALUES
('ABS00000001', 'ID10000016', 'Hadir', 'Approve', '30-08-2022', '20:10:16'),
('ABS00000002', 'ID10000012', 'Hadir', 'Approve', '30-08-2022', '20:11:20'),
('ABS00000003', 'ID10000015', 'Hadir', 'Approve', '30-08-2022', '20:12:47');

-- --------------------------------------------------------

--
-- Table structure for table `cuti`
--

CREATE TABLE `cuti` (
  `id` varchar(20) NOT NULL,
  `karyawanID` varchar(20) NOT NULL,
  `jenisCuti` varchar(50) NOT NULL,
  `tanggalMulai` varchar(50) NOT NULL,
  `tanggalAkhir` varchar(50) NOT NULL,
  `keterangan` text DEFAULT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cuti`
--

INSERT INTO `cuti` (`id`, `karyawanID`, `jenisCuti`, `tanggalMulai`, `tanggalAkhir`, `keterangan`, `status`) VALUES
('IDC00000001', 'ID10000016', 'Cuti Tahunan', '31-08-2022', '31-08-2022', '', 'Pending'),
('IDC00000002', 'ID10000015', 'Cuti Tahunan', '31-08-2022', '31-08-2022', '', 'Approve');

-- --------------------------------------------------------

--
-- Table structure for table `gaji`
--

CREATE TABLE `gaji` (
  `id` varchar(15) NOT NULL,
  `karyawanID` varchar(15) NOT NULL,
  `jabatan` varchar(100) NOT NULL,
  `gapok` int(11) NOT NULL,
  `transport` int(11) NOT NULL,
  `makan` int(11) NOT NULL,
  `lembur` int(11) DEFAULT NULL,
  `bulan` varchar(20) NOT NULL,
  `tahun` int(10) NOT NULL,
  `total` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `gaji`
--

INSERT INTO `gaji` (`id`, `karyawanID`, `jabatan`, `gapok`, `transport`, `makan`, `lembur`, `bulan`, `tahun`, `total`) VALUES
('IDG00000001', 'ID10000016', 'Admin', 4600000, 300000, 200000, 0, 'Agustus', 2022, '5100000'),
('IDG00000002', 'ID10000012', 'Admin', 4600000, 300000, 200000, 40000, 'Agustus', 2022, '5140000'),
('IDG00000003', 'ID10000015', 'Project Manager', 7000000, 300000, 250000, 0, 'Agustus', 2022, '7550000');

-- --------------------------------------------------------

--
-- Table structure for table `identitas`
--

CREATE TABLE `identitas` (
  `id` varchar(11) NOT NULL,
  `jenisKelamin` varchar(10) NOT NULL,
  `tempatLahir` varchar(100) NOT NULL,
  `tanggalLahir` varchar(20) NOT NULL,
  `handphone` varchar(20) NOT NULL,
  `status` varchar(50) NOT NULL,
  `agama` varchar(50) NOT NULL,
  `noKtp` varchar(20) NOT NULL,
  `alamatKtp` text NOT NULL,
  `domisili` text DEFAULT NULL,
  `user_id` varchar(20) NOT NULL,
  `created_by` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `identitas`
--

INSERT INTO `identitas` (`id`, `jenisKelamin`, `tempatLahir`, `tanggalLahir`, `handphone`, `status`, `agama`, `noKtp`, `alamatKtp`, `domisili`, `user_id`, `created_by`) VALUES
('ID10000012', 'Perempuan', 'Bandung', '16-08-1994', '08571234566', 'Lajang', 'Islam', '1234567890123456', 'Bandung Barat', '', 'ID10000012', 'ID10000011'),
('ID10000013', 'Perempuan', 'Jakarta', '05-08-2022', '08581234567', 'Lajang', 'Islam', '0787123178327', 'Jakarta', '', 'ID10000013', 'ID10000011'),
('ID10000014', 'Laki-laki', 'Tangerang', '10-08-2022', '0234890034', 'Lajang', 'Islam', '2903420934820', 'Cilegon', '', 'ID10000014', 'ID10000011'),
('ID10000015', 'Laki-laki', 'Cirebon', '06-08-2022', '0234294987', 'Lajang', 'Islam', '768236497923', 'Cirebon', '', 'ID10000015', 'ID10000011'),
('ID10000016', 'Laki-laki', 'Jakarta', '04-08-2022', '032482093', 'Lajang', 'Islam', '203480923849', 'Jakarta', '', 'ID10000016', 'ID10000011');

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `id` varchar(20) NOT NULL,
  `jabatan` varchar(128) NOT NULL,
  `gapok` varchar(50) NOT NULL,
  `transport` varchar(50) NOT NULL,
  `meals` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`id`, `jabatan`, `gapok`, `transport`, `meals`) VALUES
('IDJ00000001', 'Direktur', '10000000', '500000', '1000000'),
('IDJ00000002', 'Admin', '4600000', '300000', '200000'),
('IDJ00000003', 'Staff', '5000000', '250000', '250000'),
('IDJ00000004', 'Project Manager', '7000000', '300000', '250000');

-- --------------------------------------------------------

--
-- Table structure for table `kontak`
--

CREATE TABLE `kontak` (
  `id` int(11) NOT NULL,
  `karyawanID` varchar(20) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `hubungan` varchar(100) NOT NULL,
  `telephone` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kontak`
--

INSERT INTO `kontak` (`id`, `karyawanID`, `nama`, `hubungan`, `telephone`) VALUES
(4, 'ID10000016', 'Ahmad', 'Ayah', '0937492384');

-- --------------------------------------------------------

--
-- Table structure for table `lembur`
--

CREATE TABLE `lembur` (
  `id` varchar(20) NOT NULL,
  `karyawanID` varchar(20) NOT NULL,
  `tanggal` varchar(20) NOT NULL,
  `lembur` varchar(10) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `lembur`
--

INSERT INTO `lembur` (`id`, `karyawanID`, `tanggal`, `lembur`, `status`) VALUES
('IDL00000001', 'ID10000012', '30-08-2022', '2', 'Approve');

-- --------------------------------------------------------

--
-- Table structure for table `pekerjaan`
--

CREATE TABLE `pekerjaan` (
  `id` int(11) NOT NULL,
  `karyawanID` varchar(20) NOT NULL,
  `perusahaan` varchar(100) NOT NULL,
  `jabatan` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  `tanggalBergabung` varchar(20) NOT NULL,
  `tanggalBerakhir` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pekerjaan`
--

INSERT INTO `pekerjaan` (`id`, `karyawanID`, `perusahaan`, `jabatan`, `status`, `tanggalBergabung`, `tanggalBerakhir`) VALUES
(5, 'ID10000016', 'PT ABC', 'Admin', 'Kontrak', '16-08-2022', '31-08-2022'),
(6, 'ID10000015', 'PT ABC', 'Project Manager', 'Kontrak', '01-08-2022', '31-08-2022'),
(7, 'ID10000014', 'PT ABC', 'Direktur', 'Kontrak', '01-08-2022', '31-08-2022'),
(8, 'ID10000013', 'PT ABC', 'Staff', 'Kontrak', '01-08-2022', '31-08-2022'),
(9, 'ID10000012', 'PT ABC', 'Admin', 'Kontrak', '01-08-2022', '01-08-2022');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan`
--

CREATE TABLE `pendidikan` (
  `id` int(11) NOT NULL,
  `karyawanID` varchar(20) NOT NULL,
  `lembaga` text NOT NULL,
  `jurusan` varchar(128) NOT NULL,
  `tahunMasuk` varchar(20) NOT NULL,
  `tahunKeluar` varchar(20) NOT NULL,
  `ipk` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pendidikan`
--

INSERT INTO `pendidikan` (`id`, `karyawanID`, `lembaga`, `jurusan`, `tahunMasuk`, `tahunKeluar`, `ipk`) VALUES
(4, 'ID10000016', 'SMA 1 Jakarta', 'IPS', '2010', '2013', '8.75'),
(5, 'ID10000015', 'SMK 2 Bandung', 'TKJ', '2011', '2014', '7.5'),
(6, 'ID10000014', 'SMA 47 Jakarta', 'IPA', '2009', '2012', '8.5'),
(7, 'ID10000013', 'Unindra', 'Informatika', '2016', '2020', '3.25');

-- --------------------------------------------------------

--
-- Table structure for table `submission`
--

CREATE TABLE `submission` (
  `id` varchar(20) NOT NULL,
  `idKaryawan` varchar(20) NOT NULL,
  `tanggal` varchar(50) NOT NULL,
  `remark` varchar(128) NOT NULL,
  `status` varchar(50) NOT NULL,
  `absensi_id` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `submission`
--

INSERT INTO `submission` (`id`, `idKaryawan`, `tanggal`, `remark`, `status`, `absensi_id`) VALUES
('IDS00000001', 'ID10000016', '30-08-2022', 'Telat', 'Pending', 'ID10000016'),
('IDS00000002', 'ID10000012', '30-08-2022', 'Pulang Cepat', 'Approve', 'ID10000012');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_user` varchar(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(128) NOT NULL,
  `role` varchar(50) NOT NULL,
  `is_active` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `name`, `email`, `password`, `role`, `is_active`) VALUES
('ID10000011', 'Admin', 'admin@mail.com', '123456', 'Admin', 1),
('ID10000012', 'Septi', 'septi@mail.com', '123456', 'Karyawan', 1),
('ID10000013', 'Sinta', 'sinta@mail.com', '123456', 'Staff HRD', 1),
('ID10000014', 'Aldi', 'aldi@mail.com', '123456', 'Karyawan', 1),
('ID10000015', 'Agus', 'agus@mail.com', '123456', 'Karyawan', 1),
('ID10000016', 'Lukim', 'lukim@mail.com', '123456', 'Karyawan', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absenkeluar`
--
ALTER TABLE `absenkeluar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `absensi`
--
ALTER TABLE `absensi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cuti`
--
ALTER TABLE `cuti`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gaji`
--
ALTER TABLE `gaji`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `identitas`
--
ALTER TABLE `identitas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `jabatan` (`jabatan`);

--
-- Indexes for table `kontak`
--
ALTER TABLE `kontak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lembur`
--
ALTER TABLE `lembur`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pekerjaan`
--
ALTER TABLE `pekerjaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pendidikan`
--
ALTER TABLE `pendidikan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `submission`
--
ALTER TABLE `submission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `absenkeluar`
--
ALTER TABLE `absenkeluar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `kontak`
--
ALTER TABLE `kontak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `pekerjaan`
--
ALTER TABLE `pekerjaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `pendidikan`
--
ALTER TABLE `pendidikan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
