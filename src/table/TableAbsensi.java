/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package table;

import auth.User;
import form.PenilaianKaryawan;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import koneksi.MyConnection;
import panel.SubmissionPanel;

/**
 *
 * @author Admin
 */
public class TableAbsensi extends javax.swing.JFrame {

    DefaultTableCellRenderer renderer;
    private DefaultTableModel tabmode;
    PreparedStatement ps;
    ResultSet rs;
    String id_user = User.getId_user();

    public SubmissionPanel submissionPanel = null;

    /**
     * Creates new form TableStaff
     */
    public TableAbsensi() {
        initComponents();
        this.setLocationRelativeTo(null);
        datatable();
    }

    private void datatable() {
        String[] Header = {"ID", "ID Karyawan", "Nama Karyawan", "Absensi", "Status", "Tanggal"};
        tabmode = new DefaultTableModel(Header, 0);
        renderer = (DefaultTableCellRenderer) tableDataAbsensi.getTableHeader().getDefaultRenderer();
        renderer.setHorizontalAlignment(JLabel.LEFT);
//        String statusString = "Pending";
        try {

            String query = "SELECT\n"
                    + "     absensi.`id` AS absensi_id,\n"
                    + "     absensi.`karyawanID` AS absensi_karyawanID,\n"
                    + "     absensi.`absensi` AS absensi_absensi,\n"
                    + "     absensi.`status` AS absensi_status,\n"
                    + "     absensi.`date` AS absensi_date,\n"
                    + "     absensi.`time` AS absensi_time,\n"
                    + "     identitas.`id` AS identitas_id,\n"
                    + "     identitas.`user_id` AS identitas_user_id,\n"
                    + "     users.`id_user` AS users_id_user,\n"
                    + "     users.`name` AS users_name,\n"
                    + "     users.`email` AS users_email,\n"
                    + "     users.`role` AS users_role,\n"
                    + "     users.`is_active` AS users_is_active\n"
                    + "FROM\n"
                    + "     `identitas` identitas INNER JOIN `users` users ON identitas.`user_id` = users.`id_user`\n"
                    + "     INNER JOIN `absensi` absensi ON identitas.`id` = absensi.`karyawanID`"
                    + " WHERE users.`id_user`='" + id_user + "' order by absensi.`id` DESC";
            ps = MyConnection.getConnection().prepareStatement(query);
            rs = ps.executeQuery(query);
            while (rs.next()) {
                Object[] obj = new Object[6];
                obj[0] = rs.getString("absensi_id");
                obj[1] = rs.getString("absensi_karyawanID");
                obj[2] = rs.getString("users_name");
                obj[3] = rs.getString("absensi_absensi");
                obj[4] = rs.getString("absensi_status");
                obj[5] = rs.getString("absensi_date");

                tabmode.addRow(obj);
            }
            tableDataAbsensi.setModel(tabmode);
//            tableDataAbsensi.getColumnModel().getColumn(0).setPreferredWidth(20);
//            tableDataAbsensi.getColumnModel().getColumn(1).setPreferredWidth(100);
//            tableDataAbsensi.getColumnModel().getColumn(2).setPreferredWidth(150);
//            tableDataAbsensi.getColumnModel().getColumn(3).setPreferredWidth(100);
//            tableDataAbsensi.getColumnModel().getColumn(4).setPreferredWidth(100);
//            tableDataAbsensi.getColumnModel().getColumn(5).setPreferredWidth(100);
            tableDataAbsensi.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
        } catch (Exception e) {
            showMessage("data gagal dipanggil" + e);
        }
    }

    private void showMessage(String message) {
        JOptionPane.showMessageDialog(null, message);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tableDataAbsensi = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jLabelTitle = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        tableDataAbsensi.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tableDataAbsensi.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableDataAbsensiMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tableDataAbsensi);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Tabel Absensi");

        jLabelTitle.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelTitle.setText("Data Absensi");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 707, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabelTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(10, 10, 10))
            .addComponent(jSeparator1)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jLabelTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jLabel1)
                .addGap(13, 13, 13)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 293, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tableDataAbsensiMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableDataAbsensiMouseClicked
        try {
            // TODO add your handling code here:
            String title = jLabelTitle.getText();
            int tabelData = tableDataAbsensi.getSelectedRow();
            
            submissionPanel.absensiID = tableDataAbsensi.getValueAt(tabelData, 1).toString();
            submissionPanel.tanggalAbsensi = tableDataAbsensi.getValueAt(tabelData, 5).toString();
            submissionPanel.itemTerpilih();
            this.dispose();
        } catch (ParseException ex) {
            Logger.getLogger(TableAbsensi.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_tableDataAbsensiMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TableAbsensi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TableAbsensi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TableAbsensi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TableAbsensi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TableAbsensi().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    public javax.swing.JLabel jLabelTitle;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable tableDataAbsensi;
    // End of variables declaration//GEN-END:variables
}
