/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package panel;

import auth.User;
import form.IdentitasForm;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import koneksi.MyConnection;

/**
 *
 * @author muham
 */
public class IdentitasPanel extends javax.swing.JPanel {

    private DefaultTableModel tabmode;
    DefaultTableCellRenderer renderer;
    String id_user = User.getId_user();
    PreparedStatement ps;
    ResultSet rs;
    public String idIdentitas;
    IdentitasForm RowData = new IdentitasForm();
    String jenisKelamin = null;

    /**
     * Creates new form IdentitasPanel
     */
    public IdentitasPanel() {
        initComponents();
        datatable();
    }

    private void showMessage(String message) {
        JOptionPane.showMessageDialog(null, message);
    }

    private void datatable() {
        String[] Header = {"No.", "ID Identitas", "Nama", "Email", "Jenis Kelamin", "Tempat Lahir", "Tanggal Lahir", "Handphone", "Status"};
        tabmode = new DefaultTableModel(Header, 0);
        renderer = (DefaultTableCellRenderer) tableIdentitas.getTableHeader().getDefaultRenderer();
        renderer.setHorizontalAlignment(JLabel.LEFT);
        String cariitem = txtCari.getText();
        try {
            String query = "SELECT\n"
                    + "     identitas.`id` AS identitas_id,\n"
                    + "     identitas.`jenisKelamin` AS identitas_jenisKelamin,\n"
                    + "     identitas.`tempatLahir` AS identitas_tempatLahir,\n"
                    + "     identitas.`tanggalLahir` AS identitas_tanggalLahir,\n"
                    + "     identitas.`handphone` AS identitas_handphone,\n"
                    + "     identitas.`status` AS identitas_status,\n"
                    + "     identitas.`agama` AS identitas_agama,\n"
                    + "     identitas.`noKtp` AS identitas_noKtp,\n"
                    + "     identitas.`alamatKtp` AS identitas_alamatKtp,\n"
                    + "     identitas.`domisili` AS identitas_domisili,\n"
                    + "     identitas.`user_id` AS identitas_user_id,\n"
                    + "     identitas.`created_by` AS identitas_created_by,\n"
                    + "     users.`id_user` AS users_id_user,\n"
                    + "     users.`name` AS users_name,\n"
                    + "     users.`email` AS users_email\n"
                    + "FROM\n"
                    + "     `identitas` identitas INNER JOIN `users` users ON identitas.`user_id` = users.`id_user`"
                    + " WHERE identitas.`id` like '%" + cariitem + "%' or users.`name` like '%" + cariitem + "%' order by identitas.`id` desc";
            ps = MyConnection.getConnection().prepareStatement(query);
            ResultSet rs = ps.executeQuery(query);

            int no = 1;
            while (rs.next()) {
                Object[] obj = new Object[9];
                obj[0] = Integer.toString(no);
                obj[1] = rs.getString("identitas_id");
                obj[2] = rs.getString("users_name");
                obj[3] = rs.getString("users_email");
                obj[4] = rs.getString("identitas_jenisKelamin");
                obj[5] = rs.getString("identitas_tempatLahir");
                obj[6] = rs.getString("identitas_tanggalLahir");
                obj[7] = rs.getString("identitas_handphone");
                obj[8] = rs.getString("identitas_status");

                tabmode.addRow(obj);

                no++;
            }
            tableIdentitas.setModel(tabmode);
            tableIdentitas.getColumnModel().getColumn(0).setPreferredWidth(30);
            tableIdentitas.getColumnModel().getColumn(1).setPreferredWidth(100);
            tableIdentitas.getColumnModel().getColumn(2).setPreferredWidth(200);
            tableIdentitas.getColumnModel().getColumn(3).setPreferredWidth(200);
            tableIdentitas.getColumnModel().getColumn(4).setPreferredWidth(100);
            tableIdentitas.getColumnModel().getColumn(5).setPreferredWidth(150);
            tableIdentitas.getColumnModel().getColumn(6).setPreferredWidth(100);
            tableIdentitas.getColumnModel().getColumn(7).setPreferredWidth(100);
            tableIdentitas.getColumnModel().getColumn(8).setPreferredWidth(100);
            tableIdentitas.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "data gagal dipanggil" + e);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        btnTambah = new javax.swing.JButton();
        btnUbah = new javax.swing.JButton();
        btnHapus = new javax.swing.JButton();
        btnRefresh = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableIdentitas = new javax.swing.JTable();
        btnCari = new javax.swing.JButton();
        txtCari = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Data Identitas Karyawan");

        btnTambah.setBackground(new java.awt.Color(43, 186, 165));
        btnTambah.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnTambah.setText("Tambah");
        btnTambah.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnTambah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTambahActionPerformed(evt);
            }
        });

        btnUbah.setBackground(new java.awt.Color(0, 102, 204));
        btnUbah.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnUbah.setForeground(new java.awt.Color(255, 255, 255));
        btnUbah.setText("Ubah");
        btnUbah.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnUbah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUbahActionPerformed(evt);
            }
        });

        btnHapus.setBackground(new java.awt.Color(255, 102, 102));
        btnHapus.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnHapus.setForeground(new java.awt.Color(255, 255, 255));
        btnHapus.setText("Hapus");
        btnHapus.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHapusActionPerformed(evt);
            }
        });

        btnRefresh.setBackground(new java.awt.Color(255, 255, 51));
        btnRefresh.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnRefresh.setText("Refresh");
        btnRefresh.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });

        tableIdentitas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tableIdentitas);

        btnCari.setText("Cari");
        btnCari.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCariActionPerformed(evt);
            }
        });

        txtCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCariKeyReleased(evt);
            }
        });

        jLabel2.setText("Tabel Identitas Karyawan");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnTambah)
                        .addGap(10, 10, 10)
                        .addComponent(btnUbah)
                        .addGap(10, 10, 10)
                        .addComponent(btnHapus)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnRefresh))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 599, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtCari, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCari)))
                .addGap(10, 10, 10))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnTambah)
                    .addComponent(btnUbah)
                    .addComponent(btnHapus)
                    .addComponent(btnRefresh))
                .addGap(25, 25, 25)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCari)
                    .addComponent(txtCari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(10, 10, 10)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 354, Short.MAX_VALUE)
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnTambahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTambahActionPerformed
        // TODO add your handling code here:
        IdentitasForm form = new IdentitasForm();
        form.pack();
        form.setVisible(true);
        form.setLocationRelativeTo(null);
        form.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        form.btnUbah.setVisible(false);
    }//GEN-LAST:event_btnTambahActionPerformed

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        // TODO add your handling code here:
        datatable();
    }//GEN-LAST:event_btnRefreshActionPerformed

    private void btnUbahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUbahActionPerformed
        // TODO add your handling code here:
        int in = tableIdentitas.getSelectedRowCount();
        if (in == 0) {
            showMessage("Mohon pilih data terlebih dahulu!");
        } else {
            int ok = JOptionPane.showConfirmDialog(null, "Ubah Data", "Message", JOptionPane.YES_NO_OPTION);
            if (ok == 0) {
                int index = tableIdentitas.getSelectedRow();
                TableModel model = tableIdentitas.getModel();
                idIdentitas = model.getValueAt(index, 1).toString();
                RowData.setVisible(true);
                RowData.pack();
                RowData.setLocationRelativeTo(null);
                RowData.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                RowData.btnSimpan.setVisible(false);
                RowData.id_user = idIdentitas;
                try {
                    String query = "SELECT\n"
                            + "     identitas.`id` AS identitas_id,\n"
                            + "     identitas.`jenisKelamin` AS identitas_jenisKelamin,\n"
                            + "     identitas.`tempatLahir` AS identitas_tempatLahir,\n"
                            + "     identitas.`tanggalLahir` AS identitas_tanggalLahir,\n"
                            + "     identitas.`handphone` AS identitas_handphone,\n"
                            + "     identitas.`status` AS identitas_status,\n"
                            + "     identitas.`agama` AS identitas_agama,\n"
                            + "     identitas.`noKtp` AS identitas_noKtp,\n"
                            + "     identitas.`alamatKtp` AS identitas_alamatKtp,\n"
                            + "     identitas.`domisili` AS identitas_domisili,\n"
                            + "     identitas.`user_id` AS identitas_user_id,\n"
                            + "     identitas.`created_by` AS identitas_created_by,\n"
                            + "     users.`id_user` AS users_id_user,\n"
                            + "     users.`name` AS users_name,\n"
                            + "     users.`email` AS users_email\n"
                            + "FROM\n"
                            + "     `identitas` identitas INNER JOIN `users` users ON identitas.`user_id` = users.`id_user`"
                            + " WHERE identitas.`id`='" + idIdentitas + "'";
                    ps = MyConnection.getConnection().prepareStatement(query);
                    rs = ps.executeQuery(query);
                    while (rs.next()) {
                        RowData.txtName.setText(rs.getString("users_name"));
                        RowData.txtEmail.setText(rs.getString("users_email"));
                        jenisKelamin = rs.getString("identitas_jenisKelamin");
                        if (jenisKelamin.equals("Laki-laki")) {
                            RowData.rLaki.setSelected(true);
                        } else {
                            RowData.rPerempuan.setSelected(true);
                        }
                        RowData.txtTempatLahir.setText(rs.getString("identitas_tempatLahir"));
                        String tanggal = rs.getString("identitas_tanggalLahir");
                        Date date = new SimpleDateFormat("dd-MM-yyyy").parse(tanggal);
                        RowData.txtTanggalLahir.setDate(date);
                        RowData.txtHandphone.setText(rs.getString("identitas_handphone"));
                        RowData.comboStatus.setSelectedItem(rs.getString("identitas_status"));
                        RowData.comboAgama.setSelectedItem(rs.getString("identitas_agama"));
                        RowData.txtNoKtp.setText(rs.getString("identitas_noKtp"));
                        RowData.txtAlamatKtp.setText(rs.getString("identitas_alamatKtp"));
                        RowData.txtDomisili.setText(rs.getString("identitas_domisili"));
                    }
                } catch (Exception e) {
                    showMessage("Data gagal di ubah! " + e);
                }
            }
            datatable();
        }
    }//GEN-LAST:event_btnUbahActionPerformed

    private void btnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHapusActionPerformed
        // TODO add your handling code here:
        int in = tableIdentitas.getSelectedRowCount();
        if (in == 0) {
            showMessage("Mohon pilih data terlebih dahulu!");
        } else {
            int ok = JOptionPane.showConfirmDialog(null, "Hapus", "konfirmasi dialog", JOptionPane.YES_NO_OPTION);

            if (ok == 0) {
                int index = tableIdentitas.getSelectedRow();
                DefaultTableModel model = (DefaultTableModel) tableIdentitas.getModel();
                String reg_number = model.getValueAt(index, 1).toString();
                try {
                    String query = "DELETE FROM `identitas` WHERE id='" + reg_number + "'";
                    PreparedStatement stat = MyConnection.getConnection().prepareStatement(query);
                    stat.executeUpdate();
                    showMessage("Data berhasil dihapus!");
                } catch (SQLException e) {
                    showMessage("Data gagal dihapus! " + e);
                }
            }

            datatable();
        }
    }//GEN-LAST:event_btnHapusActionPerformed

    private void txtCariKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCariKeyReleased
        // TODO add your handling code here:
        datatable();
    }//GEN-LAST:event_txtCariKeyReleased

    private void btnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCariActionPerformed
        // TODO add your handling code here:
        datatable();
    }//GEN-LAST:event_btnCariActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCari;
    private javax.swing.JButton btnHapus;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JButton btnTambah;
    private javax.swing.JButton btnUbah;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTable tableIdentitas;
    private javax.swing.JTextField txtCari;
    // End of variables declaration//GEN-END:variables
}
