/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package panel;

import auth.User;
import java.awt.event.KeyEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import koneksi.MyConnection;

/**
 *
 * @author Admin
 */
public class CutiPanel extends javax.swing.JPanel {

    String id_user = User.getId_user();
    PreparedStatement ps;
    ResultSet rs;
    private DefaultTableModel tabmode;
    DefaultTableCellRenderer renderer;

    /**
     * Creates new form DataBarangPanel
     */
    public CutiPanel() {
        initComponents();
        datatable();
        autonumber();
        aktif();
        txtId.setEnabled(false);
        txtIdKaryawan.setEnabled(false);
        txtNama.setEnabled(false);
        kosong();
//        datatable();
    }

    private void kosong() {
        dateMulai.setDate(null);
        dateAkhir.setDate(null);
        txtKeterangan.setText("");
    }

    private void aktif() {
        try {
            String query = "SELECT * FROM `users` WHERE id_user='" + id_user + "'";
            ps = MyConnection.getConnection().prepareStatement(query);
            rs = ps.executeQuery(query);
            while (rs.next()) {
                txtIdKaryawan.setText(rs.getString("id_user"));
                txtNama.setText(rs.getString("name"));
            }
        } catch (Exception e) {
            showMessage("Error! " + e);
        }
    }

    private void autonumber() {
        try {
            String query = "select MAX(RIGHT(id,8)) AS NO  from cuti order by id desc";;
            ps = MyConnection.getConnection().prepareStatement(query);
            rs = ps.executeQuery(query);
            while (rs.next()) {
                if (rs.first() == false) {
                    txtId.setText("IDC10000001");
                } else {
                    rs.last();
                    int auto_id = rs.getInt(1) + 1;
                    String no = String.valueOf(auto_id);
                    int Nomor = no.length();
                    //MENGATUR jumlah 0
                    for (int j = 0; j < 8 - Nomor; j++) {
                        no = "0" + no;
                    }
                    txtId.setText("IDC" + no);
                }
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();//penanganan masalah
        }

    }

    private void simpanData() {
        String status = "Pending";
        String query = "INSERT INTO `cuti`(`id`, `karyawanID`, `jenisCuti`, `tanggalMulai`, `tanggalAkhir`, `keterangan`, `status`) VALUES  (?,?,?,?,?,?,?)";
        try {
            ps = MyConnection.getConnection().prepareStatement(query);
            ps.setString(1, txtId.getText().trim());
            ps.setString(2, txtIdKaryawan.getText().trim());
            ps.setString(3, comboJenis.getSelectedItem().toString());
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            ps.setString(4, String.valueOf(dateFormat.format(dateMulai.getDate())));
            ps.setString(5, String.valueOf(dateFormat.format(dateAkhir.getDate())));
            ps.setString(6, txtKeterangan.getText().trim());
            ps.setString(7, status);
            ps.executeUpdate();
            showMessage("Data telah ditambahkan!");
            autonumber();
            kosong();
        } catch (Exception e) {
            showMessage("Data gagal ditambahkan! " + e);
        }
    }

    private void showMessage(String message) {
        JOptionPane.showMessageDialog(null, message);
    }

    private void datatable() {
        String[] Header = {"ID", "ID Karyawan", "Nama Karyawan", "Jeni Cuti", "Tanggal Mulai", "Tanggal Akhir", "Keterangan", "Status"};
        tabmode = new DefaultTableModel(Header, 0);
        renderer = (DefaultTableCellRenderer) tableCuti.getTableHeader().getDefaultRenderer();
        renderer.setHorizontalAlignment(JLabel.LEFT);
        try {
            String query = "SELECT\n"
                    + "     cuti.`id` AS cuti_id,\n"
                    + "     cuti.`karyawanID` AS cuti_karyawanID,\n"
                    + "     cuti.`jenisCuti` AS cuti_jenisCuti,\n"
                    + "     cuti.`tanggalMulai` AS cuti_tanggalMulai,\n"
                    + "     cuti.`tanggalAkhir` AS cuti_tanggalAkhir,\n"
                    + "     cuti.`keterangan` AS cuti_keterangan,\n"
                    + "     cuti.`status` AS cuti_status,\n"
                    + "     identitas.`id` AS identitas_id,\n"
                    + "     identitas.`user_id` AS identitas_user_id,\n"
                    + "     users.`id_user` AS users_id_user,\n"
                    + "     users.`name` AS users_name\n"
                    + "FROM\n"
                    + "     `identitas` identitas INNER JOIN `users` users ON identitas.`user_id` = users.`id_user`\n"
                    + "     INNER JOIN `cuti` cuti ON identitas.`id` = cuti.`karyawanID`"
                    + " WHERE cuti.`karyawanID`='" + id_user + "' order by cuti.`id` DESC";
            ps = MyConnection.getConnection().prepareStatement(query);
            rs = ps.executeQuery(query);
            while (rs.next()) {
                Object[] obj = new Object[8];
                obj[0] = rs.getString("cuti_id");
                obj[1] = rs.getString("cuti_karyawanID");
                obj[2] = rs.getString("users_name");
                obj[3] = rs.getString("cuti_jenisCuti");
                obj[4] = rs.getString("cuti_tanggalMulai");
                obj[5] = rs.getString("cuti_tanggalAkhir");
                obj[6] = rs.getString("cuti_keterangan");
                obj[7] = rs.getString("cuti_status");

                tabmode.addRow(obj);
            }
            tableCuti.setModel(tabmode);
            tableCuti.getColumnModel().getColumn(0).setPreferredWidth(80);
            tableCuti.getColumnModel().getColumn(1).setPreferredWidth(100);
            tableCuti.getColumnModel().getColumn(2).setPreferredWidth(150);
            tableCuti.getColumnModel().getColumn(3).setPreferredWidth(100);
            tableCuti.getColumnModel().getColumn(4).setPreferredWidth(100);
            tableCuti.getColumnModel().getColumn(5).setPreferredWidth(100);
            tableCuti.getColumnModel().getColumn(6).setPreferredWidth(150);
            tableCuti.getColumnModel().getColumn(7).setPreferredWidth(100);
            tableCuti.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
        } catch (Exception e) {
            showMessage("data gagal dipanggil" + e);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableCuti = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        txtNama = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        btn_simpan = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        txtIdKaryawan = new javax.swing.JTextField();
        comboJenis = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        dateMulai = new com.toedter.calendar.JDateChooser();
        jLabel7 = new javax.swing.JLabel();
        dateAkhir = new com.toedter.calendar.JDateChooser();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtId = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtKeterangan = new javax.swing.JTextArea();

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Cuti");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Tabel Data Cuti");

        tableCuti.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tableCuti.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableCutiMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tableCuti);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel3.setText("Nama");

        txtNama.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        jLabel4.setText("Jenis Cuti");

        btn_simpan.setBackground(new java.awt.Color(43, 186, 165));
        btn_simpan.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_simpan.setForeground(new java.awt.Color(255, 255, 255));
        btn_simpan.setText("Simpan");
        btn_simpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_simpanActionPerformed(evt);
            }
        });

        jLabel5.setText("ID Karyawan");

        txtIdKaryawan.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        comboJenis.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Cuti Tahunan", "Cuti Menikah", "Cuti Keluarga Meninggal", "Cuti Melahirkan" }));

        jLabel6.setText("Tanggal Mulai");

        dateMulai.setDateFormatString("dd-MM-yyyy");

        jLabel7.setText("Tanggal Akhir");

        dateAkhir.setDateFormatString("dd-MM-yyyy");

        jLabel8.setText("Keterangan");

        jLabel9.setText("ID");

        txtId.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        txtKeterangan.setColumns(20);
        txtKeterangan.setRows(1);
        txtKeterangan.setTabSize(1);
        jScrollPane2.setViewportView(txtKeterangan);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, 75, Short.MAX_VALUE))
                        .addGap(8, 8, 8)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtId, javax.swing.GroupLayout.DEFAULT_SIZE, 98, Short.MAX_VALUE)
                            .addComponent(txtIdKaryawan))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 14, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4))
                        .addGap(8, 8, 8)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtNama, javax.swing.GroupLayout.DEFAULT_SIZE, 111, Short.MAX_VALUE)
                            .addComponent(comboJenis, 0, 1, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 14, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(8, 8, 8)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(dateAkhir, javax.swing.GroupLayout.DEFAULT_SIZE, 98, Short.MAX_VALUE)
                            .addComponent(dateMulai, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btn_simpan)))
                .addGap(10, 10, 10))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel9)
                                .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtNama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel6))
                            .addComponent(dateMulai, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel5)
                                .addComponent(txtIdKaryawan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(comboJenis, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel4)
                                .addComponent(jLabel7))
                            .addComponent(dateAkhir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jScrollPane2))
                .addGap(18, 18, 18)
                .addComponent(btn_simpan)
                .addGap(15, 15, 15))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2)
                .addGap(11, 11, 11)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 252, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void tableCutiMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableCutiMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tableCutiMouseClicked

    private void btn_simpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_simpanActionPerformed
        // TODO add your handling code here:
        simpanData();
        datatable();

    }//GEN-LAST:event_btn_simpanActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_simpan;
    private javax.swing.JComboBox<String> comboJenis;
    private com.toedter.calendar.JDateChooser dateAkhir;
    private com.toedter.calendar.JDateChooser dateMulai;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTable tableCuti;
    private javax.swing.JTextField txtId;
    private javax.swing.JTextField txtIdKaryawan;
    private javax.swing.JTextArea txtKeterangan;
    private javax.swing.JTextField txtNama;
    // End of variables declaration//GEN-END:variables

}
