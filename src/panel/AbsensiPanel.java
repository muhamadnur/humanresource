/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package panel;

import auth.User;
import java.awt.event.KeyEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import koneksi.MyConnection;

/**
 *
 * @author Admin
 */
public class AbsensiPanel extends javax.swing.JPanel {

    String id_user = User.getId_user();
    String nameUser = User.getName();
    PreparedStatement ps;
    ResultSet rs;
    private DefaultTableModel tabmode;
    DefaultTableCellRenderer renderer;
    String tanggal = "";
    String waktu = "";

    /**
     * Creates new form DataBarangPanel
     */
    public AbsensiPanel() {
        initComponents();
        datatable();
        autonumber();
        aktif();
        txtId.setEnabled(false);
        txtNama.setEnabled(false);
        showTanggal();
    }

    private void showTanggal() {
        Date nowDate = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        tanggal = simpleDateFormat.format(nowDate);
    }

    private void aktif() {
        try {
            String query = "SELECT * FROM `users` WHERE id_user='" + id_user + "'";
            ps = MyConnection.getConnection().prepareStatement(query);
            rs = ps.executeQuery(query);
            while (rs.next()) {
                txtId.setText(rs.getString("id_user"));
                txtNama.setText(rs.getString("name"));
            }
        } catch (Exception e) {
            showMessage("Error! " + e);
        }
    }

    private void autonumber() {
        try {
            String query = "select MAX(RIGHT(id,8)) AS NO  from absensi order by id desc";;
            ps = MyConnection.getConnection().prepareStatement(query);
            rs = ps.executeQuery(query);
            while (rs.next()) {
                if (rs.first() == false) {
                    jLabelID.setText("ABS10000001");
                } else {
                    rs.last();
                    int auto_id = rs.getInt(1) + 1;
                    String no = String.valueOf(auto_id);
                    int Nomor = no.length();
                    //MENGATUR jumlah 0
                    for (int j = 0; j < 8 - Nomor; j++) {
                        no = "0" + no;
                    }
                    jLabelID.setText("ABS" + no);
                }
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();//penanganan masalah
        }

    }

    private void simpanData() {
        String status = "Pending";
        String query = "INSERT INTO `absensi`(`id`, `karyawanID`, `absensi`, `status`,`date`) VALUES  (?,?,?,?,?)";
        try {
            ps = MyConnection.getConnection().prepareStatement(query);
            ps.setString(1, jLabelID.getText().trim());
            ps.setString(2, txtId.getText().trim());
            ps.setString(3, comboAbsen.getSelectedItem().toString());
            ps.setString(4, status);
            ps.setString(5, tanggal);
            ps.executeUpdate();
            showMessage("Data telah ditambahkan!");
            autonumber();
        } catch (Exception e) {
            showMessage("Data gagal ditambahkan! " + e);
        }
    }
    
    private void absenKeluar(){
        String query = "INSERT INTO `absenkeluar`(`absen_id`, `tanggal`) VALUES  (?,?)";
        try {
            ps = MyConnection.getConnection().prepareStatement(query);
            ps.setString(1, jLabelID.getText().trim());
            ps.setString(2, tanggal);
            ps.executeUpdate();
            showMessage("Data telah ditambahkan!");
            autonumber();
        } catch (Exception e) {
            showMessage("Data gagal ditambahkan! " + e);
        }
    }

    private void showMessage(String message) {
        JOptionPane.showMessageDialog(null, message);
    }

    private void datatable() {
        String[] Header = {"ID", "ID Karyawan", "Nama Karyawan", "Absensi", "Status", "Tanggal","Waktu"};
        tabmode = new DefaultTableModel(Header, 0);
        renderer = (DefaultTableCellRenderer) tableAbsensi.getTableHeader().getDefaultRenderer();
        renderer.setHorizontalAlignment(JLabel.LEFT);
        try {
            String query = "SELECT\n"
                    + "     absensi.`id` AS absensi_id,\n"
                    + "     absensi.`karyawanID` AS absensi_karyawanID,\n"
                    + "     absensi.`absensi` AS absensi_absensi,\n"
                    + "     absensi.`status` AS absensi_status,\n"
                    + "     absensi.`date` AS absensi_date,\n"
                    + "     absensi.`time` AS absensi_time,\n"
                    + "     identitas.`id` AS identitas_id,\n"
                    + "     identitas.`user_id` AS identitas_user_id,\n"
                    + "     users.`id_user` AS users_id_user,\n"
                    + "     users.`name` AS users_name,\n"
                    + "     users.`email` AS users_email,\n"
                    + "     users.`role` AS users_role,\n"
                    + "     users.`is_active` AS users_is_active\n"
                    + "FROM\n"
                    + "     `identitas` identitas INNER JOIN `users` users ON identitas.`user_id` = users.`id_user`\n"
                    + "     INNER JOIN `absensi` absensi ON identitas.`id` = absensi.`karyawanID`"
                    + " WHERE users.`id_user`='" + id_user + "' order by absensi.`id` DESC";
            ps = MyConnection.getConnection().prepareStatement(query);
            rs = ps.executeQuery(query);
            while (rs.next()) {
                Object[] obj = new Object[7];
                obj[0] = rs.getString("absensi_id");
                obj[1] = rs.getString("absensi_karyawanID");
                obj[2] = rs.getString("users_name");
                obj[3] = rs.getString("absensi_absensi");
                obj[4] = rs.getString("absensi_status");
                obj[5] = rs.getString("absensi_date");
                obj[6] = rs.getString("absensi_time");

                tabmode.addRow(obj);
            }
            tableAbsensi.setModel(tabmode);
            tableAbsensi.getColumnModel().getColumn(0).setPreferredWidth(15);
            tableAbsensi.getColumnModel().getColumn(1).setPreferredWidth(100);
            tableAbsensi.getColumnModel().getColumn(2).setPreferredWidth(150);
            tableAbsensi.getColumnModel().getColumn(3).setPreferredWidth(150);
            tableAbsensi.getColumnModel().getColumn(4).setPreferredWidth(100);
            tableAbsensi.getColumnModel().getColumn(5).setPreferredWidth(100);
            tableAbsensi.getColumnModel().getColumn(6).setPreferredWidth(100);
            tableAbsensi.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
        } catch (Exception e) {
            showMessage("data gagal dipanggil" + e);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableAbsensi = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        txtNama = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        btnAbsenMasuk = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        txtId = new javax.swing.JTextField();
        comboAbsen = new javax.swing.JComboBox<>();
        btnAbsenKeluar = new javax.swing.JButton();
        jLabelID = new javax.swing.JLabel();

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Absensi");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Tabel Data Absensi");

        tableAbsensi.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tableAbsensi.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableAbsensiMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tableAbsensi);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel3.setText("Nama");

        txtNama.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        jLabel4.setText("Absen");

        btnAbsenMasuk.setBackground(new java.awt.Color(43, 186, 165));
        btnAbsenMasuk.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnAbsenMasuk.setForeground(new java.awt.Color(255, 255, 255));
        btnAbsenMasuk.setText("Absen Masuk");
        btnAbsenMasuk.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAbsenMasuk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAbsenMasukActionPerformed(evt);
            }
        });

        jLabel5.setText("ID Karyawan");

        txtId.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        comboAbsen.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Hadir", "Izin", "Sakit" }));

        btnAbsenKeluar.setBackground(new java.awt.Color(255, 204, 51));
        btnAbsenKeluar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnAbsenKeluar.setText("Absen Keluar");
        btnAbsenKeluar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAbsenKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAbsenKeluarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addGap(8, 8, 8)
                .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel3)
                .addGap(8, 8, 8)
                .addComponent(txtNama, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel4)
                .addGap(8, 8, 8)
                .addComponent(comboAbsen, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(162, 162, 162)
                .addComponent(btnAbsenMasuk)
                .addGap(10, 10, 10)
                .addComponent(btnAbsenKeluar)
                .addGap(10, 10, 10))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtNama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)
                    .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboAbsen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAbsenMasuk)
                    .addComponent(btnAbsenKeluar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabelID.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabelID, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1)
                    .addComponent(jLabel2))
                .addContainerGap())
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabelID, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE))
                .addGap(0, 0, 0)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(jLabel2)
                .addGap(11, 11, 11)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 312, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void tableAbsensiMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableAbsensiMouseClicked
        // TODO add your handling code here:
        int selectedRow = tableAbsensi.getSelectedRow();
        DefaultTableModel model = (DefaultTableModel) tableAbsensi.getModel();
        jLabelID.setText(model.getValueAt(selectedRow, 0).toString());
    }//GEN-LAST:event_tableAbsensiMouseClicked

    private void btnAbsenMasukActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAbsenMasukActionPerformed
        // TODO add your handling code here:
        simpanData();
        datatable();

    }//GEN-LAST:event_btnAbsenMasukActionPerformed

    private void btnAbsenKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAbsenKeluarActionPerformed
        // TODO add your handling code here:
        int ok = JOptionPane.showConfirmDialog(null, "Absen Keluar", "konfirmasi dialog", JOptionPane.YES_NO_OPTION);
        if (ok == 0) {
            int in = tableAbsensi.getSelectedRowCount();
            if (in == 0) {
                showMessage("Mohon pilih data di tabel terlebih dahulu!");
            } else {
                absenKeluar();
            }
        }
    }//GEN-LAST:event_btnAbsenKeluarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAbsenKeluar;
    private javax.swing.JButton btnAbsenMasuk;
    private javax.swing.JComboBox<String> comboAbsen;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabelID;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTable tableAbsensi;
    private javax.swing.JTextField txtId;
    private javax.swing.JTextField txtNama;
    // End of variables declaration//GEN-END:variables

}
