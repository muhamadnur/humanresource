/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package panel;

import auth.User;
import java.awt.event.KeyEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import koneksi.MyConnection;
import table.TableKaryawan;

/**
 *
 * @author Admin
 */
public class PendidikanPanel extends javax.swing.JPanel {

    public String karyawanID;
    public String karyawanName;
    String nama = null;
    String idKaryawan = null;
    String namaLembaga = null;
    String jurusan = null;
    String tahunMasuk = null;
    String tahunKeluar = null;
    String ipk = null;
    String id_user = User.getId_user();
    PreparedStatement ps;
    ResultSet rs;
    private DefaultTableModel tabmode;
    DefaultTableCellRenderer renderer;

    /**
     * Creates new form DataBarangPanel
     */
    public PendidikanPanel() {
        initComponents();
        datatable();
        txtIdKaryawan.setEnabled(false);
        txtName.setEnabled(false);
        kosong();
    }

    private void kosong() {
        txtIdKaryawan.setText("");
        txtName.setText("");
        txtLembaga.setText("");
        txtJurusan.setText("");
        txtTahunKeluar.setText("");
        txtTahunMasuk.setText("");
        txtIPK.setText("");

    }

    public void itemTerpilih() {
        TableKaryawan Dpw = new TableKaryawan();
        Dpw.pendidikanPanel = this;
        txtIdKaryawan.setText(karyawanID);
        txtName.setText(karyawanName);
    }

    private void datatable() {
        String[] Header = {"ID", "ID Karyawan", "Nama", "Lembaga", "Jurusan", "Tahun Masuk", "Tahun Keluar", "IPK"};
        tabmode = new DefaultTableModel(Header, 0);
        renderer = (DefaultTableCellRenderer) tabelPendidikan.getTableHeader().getDefaultRenderer();
        renderer.setHorizontalAlignment(JLabel.LEFT);
        String cariitem = txtCari.getText();
        try {
            String query = "SELECT\n"
                    + "     pendidikan.`id` AS pendidikan_id,\n"
                    + "     pendidikan.`karyawanID` AS pendidikan_karyawanID,\n"
                    + "     pendidikan.`lembaga` AS pendidikan_lembaga,\n"
                    + "     pendidikan.`jurusan` AS pendidikan_jurusan,\n"
                    + "     pendidikan.`tahunMasuk` AS pendidikan_tahunMasuk,\n"
                    + "     pendidikan.`tahunKeluar` AS pendidikan_tahunKeluar,\n"
                    + "     pendidikan.`ipk` AS pendidikan_ipk,\n"
                    + "     identitas.`id` AS identitas_id,\n"
                    + "     identitas.`user_id` AS identitas_user_id,\n"
                    + "     users.`id_user` AS users_id_user,\n"
                    + "     users.`name` AS users_name\n"
                    + "FROM\n"
                    + "     `identitas` identitas INNER JOIN `users` users ON identitas.`user_id` = users.`id_user`\n"
                    + "     INNER JOIN `pendidikan` pendidikan ON identitas.`id` = pendidikan.`karyawanID`"
                    + "WHERE pendidikan.`karyawanID` like '%" + cariitem + "%' or users.`name` like '%" + cariitem + "%' order by pendidikan.`id` asc";
            ps = MyConnection.getConnection().prepareStatement(query);
            rs = ps.executeQuery(query);
            int no = 1;
            while (rs.next()) {
                Object[] obj = new Object[8];
                obj[0] = rs.getString("pendidikan_id");
                obj[1] = rs.getString("pendidikan_karyawanID");
                obj[2] = rs.getString("users_name");
                obj[3] = rs.getString("pendidikan_lembaga");
                obj[4] = rs.getString("pendidikan_jurusan");
                obj[5] = rs.getString("pendidikan_tahunMasuk");
                obj[6] = rs.getString("pendidikan_tahunKeluar");
                obj[7] = rs.getString("pendidikan_ipk");

                tabmode.addRow(obj);
            }
            tabelPendidikan.setModel(tabmode);
//            tabelPendidikan.getColumnModel().getColumn(0).setPreferredWidth(15);
            tabelPendidikan.getColumnModel().getColumn(0).setPreferredWidth(15);
            tabelPendidikan.getColumnModel().getColumn(1).setPreferredWidth(100);
            tabelPendidikan.getColumnModel().getColumn(2).setPreferredWidth(150);
            tabelPendidikan.getColumnModel().getColumn(3).setPreferredWidth(150);
            tabelPendidikan.getColumnModel().getColumn(4).setPreferredWidth(100);
            tabelPendidikan.getColumnModel().getColumn(5).setPreferredWidth(80);
            tabelPendidikan.getColumnModel().getColumn(6).setPreferredWidth(80);
            tabelPendidikan.getColumnModel().getColumn(7).setPreferredWidth(50);
            tabelPendidikan.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
        } catch (Exception e) {
            showMessage("data gagal dipanggil" + e);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jPanel2 = new javax.swing.JPanel();
        txtName = new javax.swing.JTextField();
        txtLembaga = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        btn_simpan = new javax.swing.JButton();
        btn_ubah = new javax.swing.JButton();
        btn_hapus = new javax.swing.JButton();
        btn_batal = new javax.swing.JButton();
        btn_cariLoket = new javax.swing.JButton();
        txtIdKaryawan = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtJurusan = new javax.swing.JTextField();
        txtTahunMasuk = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txtTahunKeluar = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txtIPK = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelPendidikan = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        txtCari = new javax.swing.JTextField();
        btnCari = new javax.swing.JButton();
        jLabelID = new javax.swing.JLabel();

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Data Pendidikan Karyawan");

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        txtName.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btn_simpan.setBackground(new java.awt.Color(43, 186, 165));
        btn_simpan.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_simpan.setForeground(new java.awt.Color(255, 255, 255));
        btn_simpan.setText("Simpan");
        btn_simpan.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_simpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_simpanActionPerformed(evt);
            }
        });

        btn_ubah.setBackground(new java.awt.Color(0, 102, 204));
        btn_ubah.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_ubah.setForeground(new java.awt.Color(255, 255, 255));
        btn_ubah.setText("Ubah");
        btn_ubah.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_ubah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ubahActionPerformed(evt);
            }
        });

        btn_hapus.setBackground(new java.awt.Color(255, 102, 102));
        btn_hapus.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_hapus.setForeground(new java.awt.Color(255, 255, 255));
        btn_hapus.setText("Hapus");
        btn_hapus.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_hapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_hapusActionPerformed(evt);
            }
        });

        btn_batal.setBackground(new java.awt.Color(255, 102, 102));
        btn_batal.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_batal.setForeground(new java.awt.Color(255, 255, 255));
        btn_batal.setText("Batal");
        btn_batal.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_batal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_batalActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_simpan, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn_ubah, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn_hapus, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn_batal, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(btn_simpan)
                .addGap(10, 10, 10)
                .addComponent(btn_ubah)
                .addGap(10, 10, 10)
                .addComponent(btn_hapus)
                .addGap(10, 10, 10)
                .addComponent(btn_batal)
                .addGap(10, 10, 10))
        );

        btn_cariLoket.setText("Cari");
        btn_cariLoket.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_cariLoket.setMaximumSize(new java.awt.Dimension(73, 20));
        btn_cariLoket.setMinimumSize(new java.awt.Dimension(73, 20));
        btn_cariLoket.setPreferredSize(new java.awt.Dimension(73, 20));
        btn_cariLoket.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cariLoketActionPerformed(evt);
            }
        });

        txtIdKaryawan.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        jLabel3.setText("ID Karyawan");

        jLabel4.setText("Nama");

        jLabel5.setText("Nama Lembaga");

        jLabel7.setText("Jurusan");

        jLabel8.setText("Tahun Masuk");

        jLabel9.setText("Tahun Keluar");

        jLabel10.setText("IPK");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jLabel7)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtLembaga, javax.swing.GroupLayout.DEFAULT_SIZE, 181, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(txtIdKaryawan, javax.swing.GroupLayout.DEFAULT_SIZE, 102, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_cariLoket, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txtName)
                    .addComponent(txtJurusan)
                    .addComponent(txtTahunMasuk)
                    .addComponent(txtTahunKeluar)
                    .addComponent(txtIPK))
                .addGap(10, 10, 10))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(btn_cariLoket, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtIdKaryawan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 1, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtLembaga, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtJurusan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTahunMasuk, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(txtTahunKeluar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(txtIPK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        tabelPendidikan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tabelPendidikan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabelPendidikanMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tabelPendidikan);

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel2.setText("Tabel Data Pendidikan Karyawan");

        txtCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCariKeyReleased(evt);
            }
        });

        btnCari.setText("Search");
        btnCari.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnCari.setMaximumSize(new java.awt.Dimension(65, 20));
        btnCari.setMinimumSize(new java.awt.Dimension(65, 20));
        btnCari.setPreferredSize(new java.awt.Dimension(65, 20));
        btnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCariActionPerformed(evt);
            }
        });

        jLabelID.setForeground(new java.awt.Color(255, 255, 255));
        jLabelID.setText("jLabel6");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 217, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabelID)
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jScrollPane1)
                                .addGap(6, 6, 6))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(468, 468, 468)))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(txtCari, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnCari, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelID))
                .addGap(0, 0, 0)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtCari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btn_simpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_simpanActionPerformed
        // TODO add your handling code here:
        idKaryawan = txtIdKaryawan.getText();
        nama = txtName.getText();
        namaLembaga = txtLembaga.getText();
        jurusan = txtJurusan.getText();
        tahunMasuk = txtTahunMasuk.getText();
        tahunKeluar = txtTahunKeluar.getText();
        ipk = txtIPK.getText();
        if (idKaryawan.isEmpty()) {
            showMessage("ID Karyawan tidak boleh kosong!");
            btn_cariLoket.requestFocus();
        } else if (nama.isEmpty()) {
            showMessage("Nama tidak boleh kosong!");
            txtName.requestFocus();
        } else if (namaLembaga.isEmpty()) {
            showMessage("Nama Lembaga tidak boleh kosong!");
            txtLembaga.requestFocus();
        } else if (jurusan.isEmpty()) {
            showMessage("Jurusan tidak boleh kosong!");
            txtJurusan.requestFocus();
        } else if (tahunMasuk.isEmpty()) {
            showMessage("Tahun Masuk tidak boleh kosong!");
            txtTahunMasuk.requestFocus();
        } else if (tahunMasuk.isEmpty()) {
            showMessage("Tahun Keluar tidak boleh kosong!");
            txtTahunKeluar.requestFocus();
        } else if (ipk.isEmpty()) {
            showMessage("IPK tidak boleh kosong!");
            txtIPK.requestFocus();
        } else {
            String query = "INSERT INTO `pendidikan`(`karyawanID`, `lembaga`, `jurusan`, `tahunMasuk`, `tahunKeluar`, `ipk`) VALUES (?,?,?,?,?,?)";
            try {
                ps = MyConnection.getConnection().prepareStatement(query);
                ps.setString(1, idKaryawan);
                ps.setString(2, namaLembaga);
                ps.setString(3, jurusan);
                ps.setString(4, tahunMasuk);
                ps.setString(5, tahunKeluar);
                ps.setString(6, ipk);
                ps.executeUpdate();
                showMessage("Data telah ditambahkan!");
                kosong();
            } catch (Exception e) {
                showMessage("Data gagal ditambahkan! " + e);
            }
            datatable();
        }
    }//GEN-LAST:event_btn_simpanActionPerformed

    private void btn_ubahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ubahActionPerformed
        // TODO add your handling code here:
        idKaryawan = txtIdKaryawan.getText();
        nama = txtName.getText();
        namaLembaga = txtLembaga.getText();
        jurusan = txtJurusan.getText();
        tahunMasuk = txtTahunMasuk.getText();
        tahunKeluar = txtTahunKeluar.getText();
        ipk = txtIPK.getText();
        if (idKaryawan.isEmpty()) {
            showMessage("ID Karyawan tidak boleh kosong!");
            btn_cariLoket.requestFocus();
        } else if (nama.isEmpty()) {
            showMessage("Nama tidak boleh kosong!");
            txtName.requestFocus();
        } else if (namaLembaga.isEmpty()) {
            showMessage("Nama Lembaga tidak boleh kosong!");
            txtLembaga.requestFocus();
        } else if (jurusan.isEmpty()) {
            showMessage("Jurusan tidak boleh kosong!");
            txtJurusan.requestFocus();
        } else if (tahunMasuk.isEmpty()) {
            showMessage("Tahun Masuk tidak boleh kosong!");
            txtTahunMasuk.requestFocus();
        } else if (tahunMasuk.isEmpty()) {
            showMessage("Tahun Keluar tidak boleh kosong!");
            txtTahunKeluar.requestFocus();
        } else if (ipk.isEmpty()) {
            showMessage("IPK tidak boleh kosong!");
            txtIPK.requestFocus();
        } else {
            String query = "UPDATE `pendidikan` SET `karyawanID`=?,`lembaga`=?,`jurusan`=?,`tahunMasuk`=?,`tahunKeluar`=?,`ipk`=? WHERE `id`='" + jLabelID.getText() + "'";
            try {
                ps = MyConnection.getConnection().prepareStatement(query);
                ps.setString(1, idKaryawan);
                ps.setString(2, namaLembaga);
                ps.setString(3, jurusan);
                ps.setString(4, tahunMasuk);
                ps.setString(5, tahunKeluar);
                ps.setString(6, ipk);
                ps.executeUpdate();
                showMessage("Data berhasil di ubah!");
                kosong();
            } catch (Exception e) {
                showMessage("Data gagal di ubah! " + e);
            }
            datatable();
        }
    }//GEN-LAST:event_btn_ubahActionPerformed

    private void btn_batalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_batalActionPerformed
        // TODO add your handling code here:
        kosong();
    }//GEN-LAST:event_btn_batalActionPerformed

    private void btn_hapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_hapusActionPerformed
        // TODO add your handling code here:
        int ok = JOptionPane.showConfirmDialog(null, "hapus", "konfirmasi dialog", JOptionPane.YES_NO_OPTION);
        if (ok == 0) {
            int in = tabelPendidikan.getSelectedRowCount();
            if (in == 0) {
                showMessage("Mohon pilih data di tabel terlebih dahulu!");
            } else {
                try {
                    String query = "DELETE FROM `pendidikan` WHERE `id`='" + jLabelID.getText() + "'";
                    PreparedStatement stat = MyConnection.getConnection().prepareStatement(query);
                    stat.executeUpdate();
                    showMessage("Data berhasil dihapus");
                    kosong();
                    datatable();
                } catch (SQLException e) {
                    showMessage("Data gagal dihapus" + e);
                }
            }
        }
    }//GEN-LAST:event_btn_hapusActionPerformed

    private void txtCariKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCariKeyReleased
        // TODO add your handling code here:
        datatable();
    }//GEN-LAST:event_txtCariKeyReleased

    private void btnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCariActionPerformed
        // TODO add your handling code here:
        datatable();
    }//GEN-LAST:event_btnCariActionPerformed

    private void tabelPendidikanMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabelPendidikanMouseClicked
        // TODO add your handling code here:
        int selectedRow = tabelPendidikan.getSelectedRow();
        DefaultTableModel model = (DefaultTableModel) tabelPendidikan.getModel();
        jLabelID.setText(model.getValueAt(selectedRow, 0).toString());
        txtIdKaryawan.setText(model.getValueAt(selectedRow, 1).toString());
        txtName.setText(model.getValueAt(selectedRow, 2).toString());
        txtLembaga.setText(model.getValueAt(selectedRow, 3).toString());
        txtJurusan.setText(model.getValueAt(selectedRow, 4).toString());
        txtTahunMasuk.setText(model.getValueAt(selectedRow, 5).toString());
        txtTahunKeluar.setText(model.getValueAt(selectedRow, 6).toString());
        txtIPK.setText(model.getValueAt(selectedRow, 7).toString());

        btn_simpan.setEnabled(false);
        btn_ubah.setEnabled(true);
        btn_hapus.setEnabled(true);
    }//GEN-LAST:event_tabelPendidikanMouseClicked

    private void btn_cariLoketActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cariLoketActionPerformed
        // TODO add your handling code here:
        TableKaryawan pro = new TableKaryawan();
        pro.pendidikanPanel = this;
        pro.jLabelTitle.setText("Data Karyawan");
        pro.pack();
        pro.setVisible(true);
        pro.setLocationRelativeTo(null);
        pro.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }//GEN-LAST:event_btn_cariLoketActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCari;
    private javax.swing.JButton btn_batal;
    private javax.swing.JButton btn_cariLoket;
    private javax.swing.JButton btn_hapus;
    private javax.swing.JButton btn_simpan;
    private javax.swing.JButton btn_ubah;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelID;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTable tabelPendidikan;
    private javax.swing.JTextField txtCari;
    private javax.swing.JTextField txtIPK;
    private javax.swing.JTextField txtIdKaryawan;
    private javax.swing.JTextField txtJurusan;
    private javax.swing.JTextField txtLembaga;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextField txtTahunKeluar;
    private javax.swing.JTextField txtTahunMasuk;
    // End of variables declaration//GEN-END:variables

    private void showMessage(String message) {
        JOptionPane.showMessageDialog(null, message);
    }
}
