/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package panel;

import auth.User;
import java.awt.event.KeyEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import koneksi.MyConnection;
import table.TableKaryawan;

/**
 *
 * @author Admin
 */
public class PekerjaanPanel extends javax.swing.JPanel {

    public String karyawanID;
    public String karyawanName;
    String id_user = User.getId_user();
    PreparedStatement ps;
    ResultSet rs;
    private DefaultTableModel tabmode;
    DefaultTableCellRenderer renderer;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

    /**
     * Creates new form DataBarangPanel
     */
    public PekerjaanPanel() {
        initComponents();
        datatable();
        loadJabatan();
        kosong();
        txtIdKaryawan.setEnabled(false);
        txtName.setEnabled(false);
    }

    private void datatable() {
        String[] Header = {"ID", "ID Karyawan", "Nama", "Perusahaan", "Jabatan", "Status"};
        tabmode = new DefaultTableModel(Header, 0);
        renderer = (DefaultTableCellRenderer) tablePekerjaan.getTableHeader().getDefaultRenderer();
        renderer.setHorizontalAlignment(JLabel.LEFT);
        String cariitem = txt_cari.getText();
        try {
            String query = "SELECT\n"
                    + "     pekerjaan.`id` AS pekerjaan_id,\n"
                    + "     pekerjaan.`karyawanID` AS pekerjaan_karyawanID,\n"
                    + "     pekerjaan.`perusahaan` AS pekerjaan_perusahaan,\n"
                    + "     pekerjaan.`jabatan` AS pekerjaan_jabatan,\n"
                    + "     pekerjaan.`status` AS pekerjaan_status,\n"
                    + "     pekerjaan.`tanggalBergabung` AS pekerjaan_tanggalBergabung,\n"
                    + "     pekerjaan.`tanggalBerakhir` AS pekerjaan_tanggalBerakhir,\n"
                    + "     users.`id_user` AS users_id_user,\n"
                    + "     users.`name` AS users_name,\n"
                    + "     identitas.`id` AS identitas_id,\n"
                    + "     identitas.`user_id` AS identitas_user_id\n"
                    + "FROM\n"
                    + "     `identitas` identitas INNER JOIN `users` users ON identitas.`user_id` = users.`id_user`\n"
                    + "     INNER JOIN `pekerjaan` pekerjaan ON identitas.`id` = pekerjaan.`karyawanID`"
                    + " WHERE pekerjaan.`id` like '%" + cariitem + "%' or users.`name` like '%" + cariitem + "%' order by pekerjaan.`id` desc";
            ps = MyConnection.getConnection().prepareStatement(query);
            rs = ps.executeQuery(query);
            while (rs.next()) {
                Object[] obj = new Object[6];
                obj[0] = rs.getString("pekerjaan_id");
                obj[1] = rs.getString("pekerjaan_karyawanID");
                obj[2] = rs.getString("users_name");
                obj[3] = rs.getString("pekerjaan_perusahaan");
                obj[4] = rs.getString("pekerjaan_jabatan");
                obj[5] = rs.getString("pekerjaan_status");

                tabmode.addRow(obj);
            }
            tablePekerjaan.setModel(tabmode);
            tablePekerjaan.getColumnModel().getColumn(0).setPreferredWidth(10);
            tablePekerjaan.getColumnModel().getColumn(1).setPreferredWidth(100);
            tablePekerjaan.getColumnModel().getColumn(2).setPreferredWidth(150);
            tablePekerjaan.getColumnModel().getColumn(3).setPreferredWidth(200);
            tablePekerjaan.getColumnModel().getColumn(4).setPreferredWidth(100);
            tablePekerjaan.getColumnModel().getColumn(5).setPreferredWidth(100);
            tablePekerjaan.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "data gagal dipanggil" + e);
        }
    }

    private void kosong() {
        txtIdKaryawan.setText("");
        txtName.setText("");
        txtPerusahaan.setText("");
        jDateTanggalBergabung.setDate(null);
        jDateTanggalBerakhir.setDate(null);
    }

    public void itemTerpilih() {
        TableKaryawan Dpw = new TableKaryawan();
        Dpw.pekerjaanPanel = this;
        txtIdKaryawan.setText(karyawanID);
        txtName.setText(karyawanName);
    }

    private void loadJabatan() {
        PreparedStatement stat;
        ResultSet rs;
        String query = "SELECT `jabatan` FROM `jabatan` order by id ASC";
        try {
            stat = MyConnection.getConnection().prepareStatement(query);
            rs = stat.executeQuery(query);
            comboJabatan.addItem("Pilih Jabatan");
            while (rs.next()) {
                Object[] ob = new Object[2];
                ob[0] = rs.getString(1);
                comboJabatan.addItem((String) ob[0]);
            }
            rs.close();
            stat.close();
        } catch (Exception e) {
            showMessage("Error " + e);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jPanel2 = new javax.swing.JPanel();
        txtName = new javax.swing.JTextField();
        txtPerusahaan = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        btn_simpan = new javax.swing.JButton();
        btn_ubah = new javax.swing.JButton();
        btn_hapus = new javax.swing.JButton();
        btn_batal = new javax.swing.JButton();
        btn_cariLoket = new javax.swing.JButton();
        txtIdKaryawan = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        comboJabatan = new javax.swing.JComboBox<>();
        comboStatus = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        jDateTanggalBergabung = new com.toedter.calendar.JDateChooser();
        jLabel9 = new javax.swing.JLabel();
        jDateTanggalBerakhir = new com.toedter.calendar.JDateChooser();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablePekerjaan = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        txt_cari = new javax.swing.JTextField();
        btn_cari = new javax.swing.JButton();
        jLabelID = new javax.swing.JLabel();

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Data Pekerjaan");

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        txtName.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btn_simpan.setBackground(new java.awt.Color(43, 186, 165));
        btn_simpan.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_simpan.setForeground(new java.awt.Color(255, 255, 255));
        btn_simpan.setText("Simpan");
        btn_simpan.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_simpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_simpanActionPerformed(evt);
            }
        });

        btn_ubah.setBackground(new java.awt.Color(0, 102, 204));
        btn_ubah.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_ubah.setForeground(new java.awt.Color(255, 255, 255));
        btn_ubah.setText("Ubah");
        btn_ubah.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_ubah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ubahActionPerformed(evt);
            }
        });

        btn_hapus.setBackground(new java.awt.Color(255, 102, 102));
        btn_hapus.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_hapus.setForeground(new java.awt.Color(255, 255, 255));
        btn_hapus.setText("Hapus");
        btn_hapus.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_hapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_hapusActionPerformed(evt);
            }
        });

        btn_batal.setBackground(new java.awt.Color(255, 102, 102));
        btn_batal.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_batal.setForeground(new java.awt.Color(255, 255, 255));
        btn_batal.setText("Batal");
        btn_batal.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_batal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_batalActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_simpan, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn_ubah, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn_hapus, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn_batal, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(btn_simpan)
                .addGap(10, 10, 10)
                .addComponent(btn_ubah)
                .addGap(10, 10, 10)
                .addComponent(btn_hapus)
                .addGap(10, 10, 10)
                .addComponent(btn_batal)
                .addGap(10, 10, 10))
        );

        btn_cariLoket.setText("Cari");
        btn_cariLoket.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_cariLoket.setMaximumSize(new java.awt.Dimension(73, 20));
        btn_cariLoket.setMinimumSize(new java.awt.Dimension(73, 20));
        btn_cariLoket.setPreferredSize(new java.awt.Dimension(73, 20));
        btn_cariLoket.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cariLoketActionPerformed(evt);
            }
        });

        txtIdKaryawan.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        jLabel3.setText("ID Karyawan");

        jLabel4.setText("Nama");

        jLabel5.setText("Perusahaan");

        jLabel6.setText("Jabatan");

        jLabel7.setText("Status Pekerjaan");

        comboStatus.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Kontrak", "Freelance", "Magang", "Karyawan Tetap" }));

        jLabel8.setText("Tanggal Bergabung");

        jLabel9.setText("Tanggal Berakhir");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jLabel9))
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtPerusahaan, javax.swing.GroupLayout.DEFAULT_SIZE, 181, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(txtIdKaryawan, javax.swing.GroupLayout.DEFAULT_SIZE, 102, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_cariLoket, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txtName)
                    .addComponent(comboJabatan, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(comboStatus, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jDateTanggalBergabung, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jDateTanggalBerakhir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(10, 10, 10))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(btn_cariLoket, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtIdKaryawan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 1, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPerusahaan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(comboJabatan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(comboStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jDateTanggalBergabung, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9)
                    .addComponent(jDateTanggalBerakhir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        tablePekerjaan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablePekerjaan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablePekerjaanMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablePekerjaan);

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel2.setText("Tabel Data Pekerjaan");

        txt_cari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txt_cariKeyReleased(evt);
            }
        });

        btn_cari.setText("Search");
        btn_cari.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_cari.setMaximumSize(new java.awt.Dimension(65, 20));
        btn_cari.setMinimumSize(new java.awt.Dimension(65, 20));
        btn_cari.setPreferredSize(new java.awt.Dimension(65, 20));
        btn_cari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cariActionPerformed(evt);
            }
        });

        jLabelID.setForeground(new java.awt.Color(255, 255, 255));
        jLabelID.setText("jLabel10");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jScrollPane1)
                                .addGap(6, 6, 6))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(468, 468, 468)))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(txt_cari, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn_cari, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabelID)
                        .addContainerGap())))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelID))
                .addGap(0, 0, 0)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txt_cari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_cari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btn_simpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_simpanActionPerformed
        // TODO add your handling code here:
        String query = "INSERT INTO `pekerjaan`(`karyawanID`, `perusahaan`, `jabatan`, `status`, `tanggalBergabung`, `tanggalBerakhir`) VALUES (?,?,?,?,?,?)";
        try {
            ps = MyConnection.getConnection().prepareStatement(query);
            ps.setString(1, txtIdKaryawan.getText().trim());
            ps.setString(2, txtPerusahaan.getText().trim());
            ps.setString(3, comboJabatan.getSelectedItem().toString());
            ps.setString(4, comboStatus.getSelectedItem().toString());
            String tanggalBergabung = String.valueOf(dateFormat.format(jDateTanggalBergabung.getDate()));
            ps.setString(5, tanggalBergabung);
            String tanggalBerakhir = String.valueOf(dateFormat.format(jDateTanggalBerakhir.getDate()));
            ps.setString(6, tanggalBerakhir);
            ps.executeUpdate();
            showMessage("Data telah ditambahkan!");
            kosong();

        } catch (Exception e) {
            showMessage("Data gagal ditambahkan! " + e);
        }
        datatable();
    }//GEN-LAST:event_btn_simpanActionPerformed

    private void btn_ubahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ubahActionPerformed
        // TODO add your handling code here:
        String query = "UPDATE `pekerjaan` SET `karyawanID`=?,`perusahaan`=?,`jabatan`=?,`status`=?,`tanggalBergabung`=?,`tanggalBerakhir`=? WHERE `id`='" + jLabelID.getText() + "'";
        try {
            ps = MyConnection.getConnection().prepareStatement(query);
            ps.setString(1, txtIdKaryawan.getText());
            ps.setString(2, txtPerusahaan.getText());
            ps.setString(3, comboJabatan.getSelectedItem().toString());
            ps.setString(4, comboStatus.getSelectedItem().toString());
            String tanggalBergabung = String.valueOf(dateFormat.format(jDateTanggalBergabung.getDate()));
            ps.setString(5, tanggalBergabung);
            String tanggalBerakhir = String.valueOf(dateFormat.format(jDateTanggalBerakhir.getDate()));
            ps.setString(6, tanggalBerakhir);
            ps.executeUpdate();
            showMessage("Data berhasil di ubah!");
            kosong();
        } catch (Exception e) {
            showMessage("Data gagal di ubah! " + e);
        }
        datatable();
    }//GEN-LAST:event_btn_ubahActionPerformed

    private void btn_batalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_batalActionPerformed
        // TODO add your handling code here:
        kosong();
    }//GEN-LAST:event_btn_batalActionPerformed

    private void btn_hapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_hapusActionPerformed
        // TODO add your handling code here:
        int ok = JOptionPane.showConfirmDialog(null, "hapus", "konfirmasi dialog", JOptionPane.YES_NO_OPTION);
        if (ok == 0) {
            int in = tablePekerjaan.getSelectedRowCount();
            if (in == 0) {
                showMessage("Mohon pilih data di tabel terlebih dahulu!");
            } else {
                try {
                    String query = "DELETE FROM `pekerjaan` WHERE `id`='" + jLabelID.getText() + "'";
                    PreparedStatement stat = MyConnection.getConnection().prepareStatement(query);
                    stat.executeUpdate();
                    showMessage("Data berhasil dihapus");
                    kosong();
                    datatable();
                } catch (SQLException e) {
                    showMessage("Data gagal dihapus" + e);
                }
            }
        }
    }//GEN-LAST:event_btn_hapusActionPerformed

    private void txt_cariKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_cariKeyReleased
        // TODO add your handling code here:
        datatable();
    }//GEN-LAST:event_txt_cariKeyReleased

    private void btn_cariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cariActionPerformed
        // TODO add your handling code here:
        datatable();
    }//GEN-LAST:event_btn_cariActionPerformed

    private void tablePekerjaanMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablePekerjaanMouseClicked
        // TODO add your handling code here:
        int selectedRow = tablePekerjaan.getSelectedRow();
        DefaultTableModel model = (DefaultTableModel) tablePekerjaan.getModel();
        jLabelID.setText(model.getValueAt(selectedRow, 0).toString());
        txtIdKaryawan.setText(model.getValueAt(selectedRow, 1).toString());
        txtName.setText(model.getValueAt(selectedRow, 2).toString());
        txtPerusahaan.setText(model.getValueAt(selectedRow, 3).toString());
        comboJabatan.setSelectedItem(model.getValueAt(selectedRow, 4).toString());
        comboStatus.setSelectedItem(model.getValueAt(selectedRow, 5).toString());

        try {
            String query = "SELECT * FROM `pekerjaan` WHERE id='" + model.getValueAt(selectedRow, 0).toString() + "'";
            ps = MyConnection.getConnection().prepareStatement(query);
            rs = ps.executeQuery(query);
            while (rs.next()) {
                jDateTanggalBergabung.setDate(rs.getDate("tanggalBergabung"));
                jDateTanggalBerakhir.setDate(rs.getDate("tanggalBerakhir"));
            }
        } catch (Exception e) {
            showMessage("Error! " + e);
        }
//        btn_simpan.setEnabled(false);
        btn_ubah.setEnabled(true);
        btn_hapus.setEnabled(true);
    }//GEN-LAST:event_tablePekerjaanMouseClicked

    private void btn_cariLoketActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cariLoketActionPerformed
        // TODO add your handling code here:
        TableKaryawan pro = new TableKaryawan();
        pro.pekerjaanPanel = this;
        pro.jLabelTitle.setText("Data Employee");
        pro.pack();
        pro.setVisible(true);
        pro.setLocationRelativeTo(null);
        pro.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }//GEN-LAST:event_btn_cariLoketActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_batal;
    private javax.swing.JButton btn_cari;
    private javax.swing.JButton btn_cariLoket;
    private javax.swing.JButton btn_hapus;
    private javax.swing.JButton btn_simpan;
    private javax.swing.JButton btn_ubah;
    private javax.swing.JComboBox<String> comboJabatan;
    private javax.swing.JComboBox<String> comboStatus;
    private com.toedter.calendar.JDateChooser jDateTanggalBerakhir;
    private com.toedter.calendar.JDateChooser jDateTanggalBergabung;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelID;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTable tablePekerjaan;
    private javax.swing.JTextField txtIdKaryawan;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextField txtPerusahaan;
    private javax.swing.JTextField txt_cari;
    // End of variables declaration//GEN-END:variables

    private void showMessage(String message) {
        JOptionPane.showMessageDialog(null, message);
    }
}
