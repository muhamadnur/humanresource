/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package panel;

import auth.User;
import java.awt.event.KeyEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import koneksi.MyConnection;
import table.TableEmployee;

/**
 *
 * @author Admin
 */
public class DataGajiPanel extends javax.swing.JPanel {

    public String karyawanID;
    public String karyawanName;
    public String jabatan;
    public String gapok;
    public String uangTransport;
    public String uangMakan;
    String id = null;
    String nama = null;
    String jenis = null;
    String id_user = User.getId_user();
    PreparedStatement ps;
    ResultSet rs;
    private DefaultTableModel tabmode;
    DefaultTableCellRenderer renderer;
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * Creates new form DataBarangPanel
     */
    public DataGajiPanel() {
        initComponents();
        aktif();
        datatable();
        autonumber();
        kosong();
    }

    private void kosong() {
        txtIdKaryawan.setText("");
        txtNama.setText("");
        txtJabatan.setText("");
        txtTransport.setText("");
        txtGapok.setText("");
        txtMakan.setText("");
        txtLembur.setText("");
        txtTotal.setText("0");
    }

    private void aktif() {
        txtIdKaryawan.setEnabled(false);
        txtNama.setEnabled(false);
        txtJabatan.setEnabled(false);
        txtTransport.setEnabled(false);
        txtMakan.setEnabled(false);
        txtGapok.setEnabled(false);
    }

    public void itemTerpilih() {
        TableEmployee Dpw = new TableEmployee();
        Dpw.dataGajiPanel = this;
        txtIdKaryawan.setText(karyawanID);
        txtNama.setText(karyawanName);
        txtJabatan.setText(jabatan);
        txtGapok.setText(gapok);
        txtTransport.setText(uangTransport);
        txtMakan.setText(uangMakan);
    }

    private void autonumber() {
        try {
            String query = "select MAX(RIGHT(id,8)) AS NO  from gaji order by id desc";;
            ps = MyConnection.getConnection().prepareStatement(query);
            rs = ps.executeQuery(query);
            while (rs.next()) {
                if (rs.first() == false) {
                    jLabelID.setText("IDG10000001");
                } else {
                    rs.last();
                    int auto_id = rs.getInt(1) + 1;
                    String no = String.valueOf(auto_id);
                    int Nomor = no.length();
                    //MENGATUR jumlah 0
                    for (int j = 0; j < 8 - Nomor; j++) {
                        no = "0" + no;
                    }
                    jLabelID.setText("IDG" + no);
                }
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();//penanganan masalah
        }

    }

    private void showMessage(String message) {
        JOptionPane.showMessageDialog(null, message);
    }

    private void datatable() {
        String[] Header = {"ID", "ID Karyawan", "Nama", "Jabatan", "Gapok", "Transport", "Makan", "Lembur", "Bulan", "Tahun", "Total"};
        tabmode = new DefaultTableModel(Header, 0);
        renderer = (DefaultTableCellRenderer) tableDataGaji.getTableHeader().getDefaultRenderer();
        renderer.setHorizontalAlignment(JLabel.LEFT);
        String cariitem = txtCari.getText();
        try {
            String query = "SELECT\n"
                    + "     gaji.`id` AS gaji_id,\n"
                    + "     gaji.`karyawanID` AS gaji_karyawanID,\n"
                    + "     gaji.`jabatan` AS gaji_jabatan,\n"
                    + "     gaji.`gapok` AS gaji_gapok,\n"
                    + "     gaji.`transport` AS gaji_transport,\n"
                    + "     gaji.`makan` AS gaji_makan,\n"
                    + "     gaji.`lembur` AS gaji_lembur,\n"
                    + "     gaji.`bulan` AS gaji_bulan,\n"
                    + "     gaji.`tahun` AS gaji_tahun,\n"
                    + "     gaji.`total` AS gaji_total,\n"
                    + "     identitas.`id` AS identitas_id,\n"
                    + "     identitas.`user_id` AS identitas_user_id,\n"
                    + "     users.`id_user` AS users_id_user,\n"
                    + "     users.`name` AS users_name\n"
                    + "FROM\n"
                    + "     `identitas` identitas INNER JOIN `users` users ON identitas.`user_id` = users.`id_user`\n"
                    + "     INNER JOIN `gaji` gaji ON identitas.`id` = gaji.`karyawanID`"
                    + " WHERE gaji.`id` like '%" + cariitem + "%' or users.`name` like '%" + cariitem + "%' order by gaji.`id` desc";
            ps = MyConnection.getConnection().prepareStatement(query);
            rs = ps.executeQuery(query);
            while (rs.next()) {
                Object[] obj = new Object[11];
                obj[0] = rs.getString("gaji_id");
                obj[1] = rs.getString("gaji_karyawanID");
                obj[2] = rs.getString("users_name");
                obj[3] = rs.getString("gaji_jabatan");
                obj[4] = rs.getString("gaji_gapok");
                obj[5] = rs.getString("gaji_transport");
                obj[6] = rs.getString("gaji_makan");
                obj[7] = rs.getString("gaji_lembur");
                obj[8] = rs.getString("gaji_bulan");
                obj[9] = rs.getString("gaji_tahun");
                obj[10] = rs.getString("gaji_total");

                tabmode.addRow(obj);
            }
            tableDataGaji.setModel(tabmode);
//            tableDataGaji.getColumnModel().getColumn(0).setPreferredWidth(15);
//            tableDataGaji.getColumnModel().getColumn(1).setPreferredWidth(100);
//            tableDataGaji.getColumnModel().getColumn(2).setPreferredWidth(100);
//            tableDataGaji.getColumnModel().getColumn(3).setPreferredWidth(300);
//            tableDataGaji.getColumnModel().getColumn(3).setPreferredWidth(150);
            tableDataGaji.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "data gagal dipanggil" + e);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableDataGaji = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        txtNama = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtJabatan = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtIdKaryawan = new javax.swing.JTextField();
        btn_cariKaryawan = new javax.swing.JButton();
        txtTransport = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtMakan = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txtLembur = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jMonthChooser = new com.toedter.calendar.JMonthChooser();
        jLabel11 = new javax.swing.JLabel();
        jYearChooser = new com.toedter.calendar.JYearChooser();
        txtTotal = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txtGapok = new javax.swing.JTextField();
        txtCari = new javax.swing.JTextField();
        btn_cari = new javax.swing.JButton();
        btn_simpan = new javax.swing.JButton();
        btn_ubah = new javax.swing.JButton();
        btn_batal = new javax.swing.JButton();
        btn_hapus = new javax.swing.JButton();
        jLabelID = new javax.swing.JLabel();

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("Data Gaji Karyawan");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("Tabel Data Gaji");

        tableDataGaji.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tableDataGaji.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableDataGajiMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tableDataGaji);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel3.setText("Nama");

        txtNama.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        jLabel4.setText("Jabatan");

        txtJabatan.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        jLabel7.setText("Uang Transport");

        jLabel5.setText("ID Karyawan");

        txtIdKaryawan.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        btn_cariKaryawan.setText("Cari");
        btn_cariKaryawan.setMaximumSize(new java.awt.Dimension(51, 20));
        btn_cariKaryawan.setMinimumSize(new java.awt.Dimension(51, 20));
        btn_cariKaryawan.setPreferredSize(new java.awt.Dimension(51, 20));
        btn_cariKaryawan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cariKaryawanActionPerformed(evt);
            }
        });

        txtTransport.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        jLabel8.setText("Uang Makan");

        txtMakan.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        jLabel9.setText("Gapok");

        txtLembur.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtLemburKeyReleased(evt);
            }
        });

        jLabel10.setText("Bulan");

        jLabel11.setText("Tahun");

        txtTotal.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtTotal.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtTotal.setText("0");
        txtTotal.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        txtTotal.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabel12.setText("Uang Lembur");

        txtGapok.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(8, 8, 8)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(txtIdKaryawan, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_cariKaryawan, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txtNama)
                    .addComponent(txtJabatan))
                .addGap(30, 30, 30)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtMakan, javax.swing.GroupLayout.DEFAULT_SIZE, 146, Short.MAX_VALUE)
                    .addComponent(txtTransport, javax.swing.GroupLayout.DEFAULT_SIZE, 146, Short.MAX_VALUE)
                    .addComponent(txtGapok))
                .addGap(30, 30, 30)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jMonthChooser, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jYearChooser, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtLembur))
                .addGap(30, 30, 30)
                .addComponent(txtTotal, javax.swing.GroupLayout.DEFAULT_SIZE, 116, Short.MAX_VALUE)
                .addGap(10, 10, 10))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel7)
                                    .addComponent(jLabel5)
                                    .addComponent(txtIdKaryawan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btn_cariKaryawan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtTransport, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel10))
                                .addComponent(jMonthChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(10, 10, 10)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel3)
                                .addComponent(txtNama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel8)
                                .addComponent(txtMakan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel11)))
                        .addComponent(jYearChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txtTotal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel4)
                        .addComponent(txtJabatan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel9)
                        .addComponent(txtLembur, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel12)
                        .addComponent(txtGapok, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(11, 11, 11))
        );

        txtCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCariKeyReleased(evt);
            }
        });

        btn_cari.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_cari.setText("Cari");
        btn_cari.setMaximumSize(new java.awt.Dimension(55, 20));
        btn_cari.setMinimumSize(new java.awt.Dimension(55, 20));
        btn_cari.setPreferredSize(new java.awt.Dimension(55, 20));
        btn_cari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cariActionPerformed(evt);
            }
        });

        btn_simpan.setBackground(new java.awt.Color(43, 186, 165));
        btn_simpan.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_simpan.setForeground(new java.awt.Color(255, 255, 255));
        btn_simpan.setText("Simpan");
        btn_simpan.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_simpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_simpanActionPerformed(evt);
            }
        });

        btn_ubah.setBackground(new java.awt.Color(0, 102, 204));
        btn_ubah.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_ubah.setForeground(new java.awt.Color(255, 255, 255));
        btn_ubah.setText("Ubah");
        btn_ubah.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_ubah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ubahActionPerformed(evt);
            }
        });

        btn_batal.setBackground(new java.awt.Color(255, 102, 102));
        btn_batal.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_batal.setForeground(new java.awt.Color(255, 255, 255));
        btn_batal.setText("Batal");
        btn_batal.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_batal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_batalActionPerformed(evt);
            }
        });

        btn_hapus.setBackground(new java.awt.Color(255, 102, 102));
        btn_hapus.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_hapus.setForeground(new java.awt.Color(255, 255, 255));
        btn_hapus.setText("Hapus");
        btn_hapus.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_hapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_hapusActionPerformed(evt);
            }
        });

        jLabelID.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabelID.setText("jLabelID");
        jLabelID.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 850, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtCari, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8)
                        .addComponent(btn_cari, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabelID, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btn_simpan)
                        .addGap(10, 10, 10)
                        .addComponent(btn_ubah)
                        .addGap(10, 10, 10)
                        .addComponent(btn_batal)
                        .addGap(10, 10, 10)
                        .addComponent(btn_hapus)))
                .addGap(10, 10, 10))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabelID)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_simpan)
                    .addComponent(btn_ubah)
                    .addComponent(btn_batal)
                    .addComponent(btn_hapus))
                .addGap(21, 21, 21)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtCari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_cari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 313, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void tableDataGajiMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableDataGajiMouseClicked
        // TODO add your handling code here:
        int selectedRow = tableDataGaji.getSelectedRow();
        DefaultTableModel model = (DefaultTableModel) tableDataGaji.getModel();
        jLabelID.setText(model.getValueAt(selectedRow, 0).toString());
        txtIdKaryawan.setText(model.getValueAt(selectedRow, 1).toString());
        txtNama.setText(model.getValueAt(selectedRow, 2).toString());
        txtJabatan.setText(model.getValueAt(selectedRow, 3).toString());
        txtGapok.setText(model.getValueAt(selectedRow, 4).toString());
        txtTransport.setText(model.getValueAt(selectedRow, 5).toString());
        txtMakan.setText(model.getValueAt(selectedRow, 6).toString());
        txtLembur.setText(model.getValueAt(selectedRow, 7).toString());
        txtTotal.setText(model.getValueAt(selectedRow, 10).toString());
    }//GEN-LAST:event_tableDataGajiMouseClicked

    private void btn_simpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_simpanActionPerformed
        // TODO add your handling code here:
        String lembur = txtLembur.getText();

        String month;
        if (jMonthChooser.getMonth() == 0) {
            month = "Januari";
        } else if (jMonthChooser.getMonth() == 1) {
            month = "Februari";
        } else if (jMonthChooser.getMonth() == 2) {
            month = "Maret";
        } else if (jMonthChooser.getMonth() == 3) {
            month = "April";
        } else if (jMonthChooser.getMonth() == 4) {
            month = "May";
        } else if (jMonthChooser.getMonth() == 5) {
            month = "Juni";
        } else if (jMonthChooser.getMonth() == 6) {
            month = "Juli";
        } else if (jMonthChooser.getMonth() == 7) {
            month = "Agustus";
        } else if (jMonthChooser.getMonth() == 8) {
            month = "September";
        } else if (jMonthChooser.getMonth() == 9) {
            month = "Oktober";
        } else if (jMonthChooser.getMonth() == 10) {
            month = "November";
        } else {
            month = "Desember";
        }
        if (lembur.isEmpty()) {
            showMessage("Lembur tidak boleh kosong!");
            txtLembur.requestFocus();
        } else {
            String query = "INSERT INTO `gaji`(`id`, `karyawanID`, `jabatan`, `gapok`,`transport`, `makan`, `lembur`, `bulan`, `tahun`, `total`) VALUES  (?,?,?,?,?,?,?,?,?,?)";
            try {
                ps = MyConnection.getConnection().prepareStatement(query);
                ps.setString(1, jLabelID.getText().trim());
                ps.setString(2, txtIdKaryawan.getText().trim());
                ps.setString(3, txtJabatan.getText().trim());
                ps.setString(4, txtGapok.getText().trim());
                ps.setString(5, txtTransport.getText().trim());
                ps.setString(6, txtMakan.getText().trim());
                ps.setString(7, txtLembur.getText().trim());
                ps.setString(8, month);
                ps.setInt(9, jYearChooser.getYear());
                ps.setString(10, txtTotal.getText().trim());

                ps.executeUpdate();
                showMessage("Data telah ditambahkan!");
                autonumber();
                datatable();
                kosong();
                aktif();
            } catch (Exception e) {
                showMessage("Data gagal ditambahkan! " + e);
            }
        }
    }//GEN-LAST:event_btn_simpanActionPerformed

    private void btn_batalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_batalActionPerformed
        // TODO add your handling code here:
        kosong();
        autonumber();
    }//GEN-LAST:event_btn_batalActionPerformed

    private void btn_ubahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ubahActionPerformed
        // TODO add your handling code here:
        String month;
        if (jMonthChooser.getMonth() == 0) {
            month = "Januari";
        } else if (jMonthChooser.getMonth() == 1) {
            month = "Februari";
        } else if (jMonthChooser.getMonth() == 2) {
            month = "Maret";
        } else if (jMonthChooser.getMonth() == 3) {
            month = "April";
        } else if (jMonthChooser.getMonth() == 4) {
            month = "May";
        } else if (jMonthChooser.getMonth() == 5) {
            month = "Juni";
        } else if (jMonthChooser.getMonth() == 6) {
            month = "Juli";
        } else if (jMonthChooser.getMonth() == 7) {
            month = "Agustus";
        } else if (jMonthChooser.getMonth() == 8) {
            month = "September";
        } else if (jMonthChooser.getMonth() == 9) {
            month = "Oktober";
        } else if (jMonthChooser.getMonth() == 10) {
            month = "November";
        } else {
            month = "Desember";
        }
        String query = "UPDATE `gaji` SET `karyawanID`=?,`jabatan`=?,`gapok`=?,`transport`=?,`makan`=?,`lembur`=?,`bulan`=?,`tahun`=?,`total`=? WHERE `id`='" + jLabelID.getText() + "'";
        try {
            ps = MyConnection.getConnection().prepareStatement(query);
            ps.setString(1, txtIdKaryawan.getText().trim());
            ps.setString(2, txtJabatan.getText().trim());
            ps.setString(3, txtGapok.getText().trim());
            ps.setString(4, txtTransport.getText().trim());
            ps.setString(5, txtMakan.getText().trim());
            ps.setString(6, txtLembur.getText().trim());
            ps.setString(7, month);
            ps.setInt(8, jYearChooser.getYear());
            ps.setString(9, txtTotal.getText().trim());

            ps.executeUpdate();
            showMessage("Data berhasil di ubah!");
            autonumber();
            datatable();
            kosong();
            aktif();
        } catch (Exception e) {
            showMessage("Data gagal di ubah! " + e);
        }

    }//GEN-LAST:event_btn_ubahActionPerformed

    private void btn_hapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_hapusActionPerformed
        // TODO add your handling code here:
        int ok = JOptionPane.showConfirmDialog(null, "hapus", "konfirmasi dialog", JOptionPane.YES_NO_OPTION);
        if (ok == 0) {
            int in = tableDataGaji.getSelectedRowCount();
            if (in == 0) {
                showMessage("Mohon pilih data di tabel terlebih dahulu!");
            } else {
                try {
                    String query = "DELETE FROM `gaji` WHERE `id`='" + jLabelID.getText() + "'";
                    PreparedStatement stat = MyConnection.getConnection().prepareStatement(query);
                    stat.executeUpdate();
                    showMessage("Data berhasil dihapus");
                    kosong();
                    autonumber();
                    datatable();
                    aktif();
                } catch (SQLException e) {
                    showMessage("Data gagal dihapus" + e);
                }
            }
        }
    }//GEN-LAST:event_btn_hapusActionPerformed

    private void txtCariKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCariKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCariKeyReleased

    private void btn_cariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cariActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_cariActionPerformed

    private void btn_cariKaryawanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cariKaryawanActionPerformed
        // TODO add your handling code here:
        TableEmployee pro = new TableEmployee();
        pro.dataGajiPanel = this;
        pro.jLabelTitle.setText("Data Karyawan");
        pro.pack();
        pro.setVisible(true);
        pro.setLocationRelativeTo(null);
        pro.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }//GEN-LAST:event_btn_cariKaryawanActionPerformed

    private void txtLemburKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtLemburKeyReleased
        // TODO add your handling code here:
        String lembur = txtLembur.getText();
        if (lembur.isEmpty()) {
            lembur.equals(0);
        }
        String transport = txtTransport.getText();
        String makan = txtMakan.getText();
        String gapok = txtGapok.getText();
        int a = Integer.parseInt(transport);
        int b = Integer.parseInt(makan);
        int c = Integer.parseInt(gapok);
        int d = Integer.parseInt(lembur);
        int e = a + b + c+ d;
        txtTotal.setText(String.valueOf(e));
    }//GEN-LAST:event_txtLemburKeyReleased


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_batal;
    private javax.swing.JButton btn_cari;
    private javax.swing.JButton btn_cariKaryawan;
    private javax.swing.JButton btn_hapus;
    private javax.swing.JButton btn_simpan;
    private javax.swing.JButton btn_ubah;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelID;
    private com.toedter.calendar.JMonthChooser jMonthChooser;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private com.toedter.calendar.JYearChooser jYearChooser;
    private javax.swing.JTable tableDataGaji;
    private javax.swing.JTextField txtCari;
    private javax.swing.JTextField txtGapok;
    private javax.swing.JTextField txtIdKaryawan;
    private javax.swing.JTextField txtJabatan;
    private javax.swing.JTextField txtLembur;
    private javax.swing.JTextField txtMakan;
    private javax.swing.JTextField txtNama;
    private javax.swing.JLabel txtTotal;
    private javax.swing.JTextField txtTransport;
    // End of variables declaration//GEN-END:variables

}
