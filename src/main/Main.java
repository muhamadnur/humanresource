/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import auth.Login;
import auth.User;
import form.FormGantiPassword;
import form.PenilaianKaryawan;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import koneksi.MyConnection;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.swing.JRViewer;
import net.sf.jasperreports.view.JasperViewer;
import panel.AbsensiPanel;
import panel.CutiPanel;
import panel.DataAbsensiPanel;
import panel.DataCutiPanel;
import panel.DataGajiPanel;
import panel.DataLemburPanel;
import panel.DataPenggunaPanel;
import panel.DataSubmissionPanel;
import panel.GajiPanel;
import panel.IdentitasPanel;
import panel.JabatanPanel;
import panel.KontakDaruratPanel;
import panel.LemburPanel;
import panel.PekerjaanPanel;
import panel.PendidikanPanel;
import panel.SubmissionPanel;

/**
 *
 * @author Muhamad Nur Sukur
 */
public class Main extends javax.swing.JFrame {

    PreparedStatement ps;
    ResultSet rs;
    String id_user = User.getId_user();
    String name = User.getName();
    String email = User.getEmail();
    String role = User.getRole();
    JpanelLoader jpload = new JpanelLoader();
    String userMenu = String.valueOf(role);

    /**
     * Creates new form Main
     */
    public Main() {
        initComponents();
        Locale locale = new Locale("id", "ID");
        Locale.setDefault(locale);
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.setLocationRelativeTo(null);

        label_name.setText(name);
//        AbsensiPanel pro = new AbsensiPanel();
//        jpload.jPanelLoader(panelBody, pro);
        switch (userMenu) {
            case "Admin":
                DataAbsensiPanel pro = new DataAbsensiPanel();
                jpload.jPanelLoader(panelBody, pro);
                break;
            case "Staff HRD":
                DataAbsensiPanel dataAbsen = new DataAbsensiPanel();
                jpload.jPanelLoader(panelBody, dataAbsen);
                break;
            case "Karyawan":
                AbsensiPanel absen = new AbsensiPanel();
                jpload.jPanelLoader(panelBody, absen);
                break;
        }
        execute();
    }

    private void execute() {
        ImageIcon iconHome = new ImageIcon(getClass().getResource("/icon/tachometer.png"));
        ImageIcon icontransaction = new ImageIcon(getClass().getResource("/icon/transaction.png"));
        ImageIcon iconreport = new ImageIcon(getClass().getResource("/icon/report.png"));
        ImageIcon iconDatabase = new ImageIcon(getClass().getResource("/icon/database.png"));
        ImageIcon iconSubMenu = new ImageIcon(getClass().getResource("/icon/subMenu.png"));
        ImageIcon iconNext = new ImageIcon(getClass().getResource("/icon/next.png"));
        ImageIcon iconLogout = new ImageIcon(getClass().getResource("/icon/logout.png"));
        ImageIcon iconProfile = new ImageIcon(getClass().getResource("/icon/user.png"));
        ImageIcon iconKey = new ImageIcon(getClass().getResource("/icon/icon_key.png"));
        ImageIcon iconKeluar = new ImageIcon(getClass().getResource("/icon/checkout.png"));
        ImageIcon iconMasuk = new ImageIcon(getClass().getResource("/icon/checkin.png"));
        ImageIcon iconSetting = new ImageIcon(getClass().getResource("/icon/icon_setting.png"));
        ImageIcon iconHistori = new ImageIcon(getClass().getResource("/icon/icon_histori.png"));
        ImageIcon iconStok = new ImageIcon(getClass().getResource("/icon/icon_stock.png"));
        ImageIcon iconForm = new ImageIcon(getClass().getResource("/icon/icon_form.png"));

        //menu formulir
        MenuItem menuAbsensi = new MenuItem(iconSubMenu, "Absensi", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                AbsensiPanel pro = new AbsensiPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });

        MenuItem menuSubmission = new MenuItem(iconSubMenu, "Submission", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                SubmissionPanel pro = new SubmissionPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        MenuItem menuLembur = new MenuItem(iconSubMenu, "Lembur", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                LemburPanel pro = new LemburPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });

        MenuItem menuCuti = new MenuItem(iconSubMenu, "Cuti", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                CutiPanel pro = new CutiPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });

        MenuItem menuSlipGaji = new MenuItem(iconSubMenu, "Slip Gaji", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                GajiPanel pro = new GajiPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });

        MenuItem menuKaryawan = new MenuItem(iconStok, "Formulir", null, menuAbsensi, menuSubmission, menuCuti, menuLembur, menuSlipGaji);

        //menu management
        MenuItem menuDataAbsen = new MenuItem(iconSubMenu, "Data Absensi", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                DataAbsensiPanel pro = new DataAbsensiPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });

        MenuItem menuDataCuti = new MenuItem(iconSubMenu, "Data Cuti", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                DataCutiPanel pro = new DataCutiPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });

        MenuItem menuDataSubmission = new MenuItem(iconSubMenu, "Data Submission", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                DataSubmissionPanel pro = new DataSubmissionPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        MenuItem menuDataLembur = new MenuItem(iconSubMenu, "Data Lembur", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                DataLemburPanel pro = new DataLemburPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });

        MenuItem menuGaji = new MenuItem(iconSubMenu, "Gaji", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                DataGajiPanel pro = new DataGajiPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });

        MenuItem menuStaffHrd = new MenuItem(iconForm, "Management", null, menuDataAbsen, menuDataCuti, menuDataSubmission, menuDataLembur,menuGaji);

        //menu karyawan
        MenuItem menuIdentitas = new MenuItem(iconSubMenu, "Data Identitas", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                IdentitasPanel pro = new IdentitasPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });

        MenuItem menuJabatan = new MenuItem(iconSubMenu, "Data Jabatan", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                JabatanPanel pro = new JabatanPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });

        MenuItem menuPendidikan = new MenuItem(iconSubMenu, "Data Pendidikan", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                PendidikanPanel pro = new PendidikanPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });

        MenuItem menuPekerjaan = new MenuItem(iconSubMenu, "Data Pekerjaan", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                PekerjaanPanel pro = new PekerjaanPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });

        MenuItem menuKontakDarurat = new MenuItem(iconSubMenu, "Data Kontak Darurat", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                KontakDaruratPanel pro = new KontakDaruratPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });

        MenuItem menuDataKaryawan = new MenuItem(iconDatabase, "Master Data", null, menuIdentitas, menuPendidikan, menuPekerjaan, menuKontakDarurat, menuJabatan);

        MenuItem laporanDataKaryawan = new MenuItem(iconSubMenu, "Data Karyawan", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                try {
                    String namaFile = "src\\report\\DataKaryawan.jasper";
                    HashMap param = new HashMap();
                    MyConnection d = new MyConnection();
                    Connection con = d.getConnection();
                    JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
                    JasperViewer.viewReport(print, false);
                    JasperViewer.setDefaultLookAndFeelDecorated(true);
                    JRViewer viewer = new JRViewer(print);
                    Container c = getRootPane();
                    c.add(viewer);
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, e.getMessage());
                }
            }
        });
        MenuItem laporanAbsensi = new MenuItem(iconSubMenu, "Absensi", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                try {
                    String namaFile = "src\\report\\DataAbsensi.jasper";
                    HashMap param = new HashMap();
                    MyConnection d = new MyConnection();
                    Connection con = d.getConnection();
                    JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
                    JasperViewer.viewReport(print, false);
                    JasperViewer.setDefaultLookAndFeelDecorated(true);
                    JRViewer viewer = new JRViewer(print);
                    Container c = getRootPane();
                    c.add(viewer);
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, e.getMessage());
                }
            }
        });
        MenuItem laporanGaji = new MenuItem(iconSubMenu, "Gaji", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                try {
                    String namaFile = "src\\report\\DataGaji.jasper";
                    HashMap param = new HashMap();
                    MyConnection d = new MyConnection();
                    Connection con = d.getConnection();
                    JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
                    JasperViewer.viewReport(print, false);
                    JasperViewer.setDefaultLookAndFeelDecorated(true);
                    JRViewer viewer = new JRViewer(print);
                    Container c = getRootPane();
                    c.add(viewer);
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, e.getMessage());
                }
            }
        });
        MenuItem laporanSubmission = new MenuItem(iconSubMenu, "Submission", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                try {
                    String namaFile = "src\\report\\DataSubmission.jasper";
                    HashMap param = new HashMap();
                    MyConnection d = new MyConnection();
                    Connection con = d.getConnection();
                    JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
                    JasperViewer.viewReport(print, false);
                    JasperViewer.setDefaultLookAndFeelDecorated(true);
                    JRViewer viewer = new JRViewer(print);
                    Container c = getRootPane();
                    c.add(viewer);
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, e.getMessage());
                }

            }
        });
        MenuItem laporanLembur = new MenuItem(iconSubMenu, "Lembur", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                try {
                    String namaFile = "src\\report\\DataLembur.jasper";
                    HashMap param = new HashMap();
                    MyConnection d = new MyConnection();
                    Connection con = d.getConnection();
                    JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
                    JasperViewer.viewReport(print, false);
                    JasperViewer.setDefaultLookAndFeelDecorated(true);
                    JRViewer viewer = new JRViewer(print);
                    Container c = getRootPane();
                    c.add(viewer);
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, e.getMessage());
                }

            }
        });
        MenuItem laporanPenilaianKaryawan = new MenuItem(iconSubMenu, "Penilaian Karyawan", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                PenilaianKaryawan pass = new PenilaianKaryawan();
                pass.setVisible(true);
                pass.pack();
                pass.setLocationRelativeTo(null);
                pass.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            }
        });

        MenuItem menuLaporan = new MenuItem(iconreport, "Laporan", null, laporanDataKaryawan, laporanAbsensi, laporanSubmission, laporanLembur, laporanGaji, laporanPenilaianKaryawan);
        //menu data pengguna
        MenuItem menuPengguna = new MenuItem(iconSubMenu, "Data Pengguna", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                DataPenggunaPanel pro = new DataPenggunaPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });

        //menu ganti password
        MenuItem menuPassword = new MenuItem(iconSubMenu, "Ubah Password", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                FormGantiPassword pass = new FormGantiPassword();
                pass.setVisible(true);
                pass.pack();
                pass.setLocationRelativeTo(null);
                pass.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            }
        });

        MenuItem changepassword = new MenuItem(iconSetting, "Ubah Password", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                FormGantiPassword pass = new FormGantiPassword();
                pass.setVisible(true);
                pass.pack();
                pass.setLocationRelativeTo(null);
                pass.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            }
        });

        MenuItem menuSetting = new MenuItem(iconSetting, "Pengaturan", null, menuPengguna, menuPassword);

        MenuItem menuLogout = new MenuItem(iconLogout, "Log Out", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int ok = JOptionPane.showConfirmDialog(null, "Log Out?", "Message", 2, JOptionPane.YES_NO_OPTION);
                if (ok == 0) {
                    showMessage("Log out successfully!");
                    Login login = new Login();
                    login.setVisible(true);
                    login.pack();
                    login.setLocationRelativeTo(null);
                    login.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    dispose();
                }
            }
        });
        switch (userMenu) {
            case "Admin":
                addMenu(menuStaffHrd, menuDataKaryawan, menuLaporan, menuSetting, menuLogout);
                break;
            case "Staff HRD":
                addMenu(menuStaffHrd, menuDataKaryawan, menuLaporan, changepassword, menuLogout);
                break;
            case "Karyawan":
                addMenu(menuKaryawan, changepassword, menuLogout);
                break;
        }

//        addMenu(menuKaryawan, menuStaffHrd, menuDataKaryawan, menuLaporan, menuSetting, menuLogout);
    }

    private void addMenu(MenuItem... menu) {
        for (int i = 0; i < menu.length; i++) {
            menus.add(menu[i]);
            ArrayList<MenuItem> subMenu = menu[i].getSubMenu();
            for (MenuItem m : subMenu) {
                addMenu(m);
            }
        }
        menus.revalidate();
    }

    private void showMessage(String message) {
        JOptionPane.showMessageDialog(null, message);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelHeader = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        label_name = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        panelMenu = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        menus = new javax.swing.JPanel();
        panelBody = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        panelHeader.setBackground(new java.awt.Color(73, 74, 72));
        panelHeader.setPreferredSize(new java.awt.Dimension(561, 50));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/icons.png"))); // NOI18N
        jLabel1.setText("HUMAN");

        label_name.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        label_name.setForeground(new java.awt.Color(255, 255, 255));
        label_name.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        label_name.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/person.png"))); // NOI18N
        label_name.setText("Nama");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(45, 83, 237));
        jLabel2.setText("RESOURCE");

        javax.swing.GroupLayout panelHeaderLayout = new javax.swing.GroupLayout(panelHeader);
        panelHeader.setLayout(panelHeaderLayout);
        panelHeaderLayout.setHorizontalGroup(
            panelHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelHeaderLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(5, 5, 5)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(350, 350, 350)
                .addComponent(label_name, javax.swing.GroupLayout.DEFAULT_SIZE, 297, Short.MAX_VALUE)
                .addContainerGap())
        );
        panelHeaderLayout.setVerticalGroup(
            panelHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelHeaderLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(panelHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addGroup(panelHeaderLayout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addComponent(label_name, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        getContentPane().add(panelHeader, java.awt.BorderLayout.PAGE_START);

        panelMenu.setBackground(new java.awt.Color(239, 240, 237));
        panelMenu.setPreferredSize(new java.awt.Dimension(250, 384));

        jScrollPane1.setBorder(null);

        menus.setBackground(new java.awt.Color(255, 255, 255));
        menus.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        menus.setLayout(new javax.swing.BoxLayout(menus, javax.swing.BoxLayout.Y_AXIS));
        jScrollPane1.setViewportView(menus);

        javax.swing.GroupLayout panelMenuLayout = new javax.swing.GroupLayout(panelMenu);
        panelMenu.setLayout(panelMenuLayout);
        panelMenuLayout.setHorizontalGroup(
            panelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMenuLayout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 242, Short.MAX_VALUE)
                .addGap(8, 8, 8))
        );
        panelMenuLayout.setVerticalGroup(
            panelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 407, Short.MAX_VALUE)
        );

        getContentPane().add(panelMenu, java.awt.BorderLayout.LINE_START);

        panelBody.setBackground(new java.awt.Color(255, 255, 255));
        panelBody.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        javax.swing.GroupLayout panelBodyLayout = new javax.swing.GroupLayout(panelBody);
        panelBody.setLayout(panelBodyLayout);
        panelBodyLayout.setHorizontalGroup(
            panelBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 636, Short.MAX_VALUE)
        );
        panelBodyLayout.setVerticalGroup(
            panelBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 403, Short.MAX_VALUE)
        );

        getContentPane().add(panelBody, java.awt.BorderLayout.CENTER);

        setSize(new java.awt.Dimension(906, 496));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Main().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JLabel label_name;
    private javax.swing.JPanel menus;
    public static javax.swing.JPanel panelBody;
    private javax.swing.JPanel panelHeader;
    private javax.swing.JPanel panelMenu;
    // End of variables declaration//GEN-END:variables

}
