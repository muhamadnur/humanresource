/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package form;

import auth.User;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import koneksi.MyConnection;

/**
 *
 * @author muham
 */
public class IdentitasForm extends javax.swing.JFrame {

    String name = null;
    String email = null;
    String pass = "123456";
    String role = "Karyawan";
    String id = "";
    String idIdentitas = "";
    public String id_user;
    String created_by = User.getId_user();
    int is_active = 1;
    PreparedStatement ps;
    ResultSet rs;
    String jenisKelamin = null;
    String tempatLahir = null;
    String handPhone = null;
    String noKtp = null;
    String alamat = null;
    String domisili = null;
    String statusPernikahan = null;
    String agama = null;
    String tanggal = null;

    /**
     * Creates new form IdentitasForm
     */
    public IdentitasForm() {
        initComponents();
        this.setLocationRelativeTo(null);
        aktif();
        autonumberUser();
        autonumberIdentitas();
    }

    private void aktif() {
        rLaki.setSelected(true);
    }

    public boolean checkUser(String emailString) {
        PreparedStatement ps;
        ResultSet rs;
        boolean emailUser = false;
        String query = "SELECT * FROM `users` WHERE `email` =?";
        try {
            ps = MyConnection.getConnection().prepareStatement(query);
            ps.setString(1, emailString);

            rs = ps.executeQuery();

            if (rs.next()) {
                emailUser = true;
            }
        } catch (SQLException ex) {
            showMessage("Error " + ex);
        }
        return emailUser;
    }

    private void autonumberUser() {
        try {
            String query = "select MAX(RIGHT(id_user,8)) AS NO  from users order by id_user desc";;
            ps = MyConnection.getConnection().prepareStatement(query);
            rs = ps.executeQuery(query);
            while (rs.next()) {
                if (rs.first() == false) {
                    id = "ID10000001";
                } else {
                    rs.last();
                    int auto_id = rs.getInt(1) + 1;
                    String no = String.valueOf(auto_id);
                    int Nomor = no.length();
                    //MENGATUR jumlah 0
                    for (int j = 0; j < 8 - Nomor; j++) {
                        no = "0" + no;
                    }
                    id = "ID" + no;
                }
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();//penanganan masalah
        }
    }

    private void autonumberIdentitas() {
        try {
            String query = "select MAX(RIGHT(id_user,8)) AS NO  from users order by id_user desc";;
            ps = MyConnection.getConnection().prepareStatement(query);
            rs = ps.executeQuery(query);
            while (rs.next()) {
                if (rs.first() == false) {
                    idIdentitas = "ID10000001";
                } else {
                    rs.last();
                    int auto_id = rs.getInt(1) + 1;
                    String no = String.valueOf(auto_id);
                    int Nomor = no.length();
                    //MENGATUR jumlah 0
                    for (int j = 0; j < 8 - Nomor; j++) {
                        no = "0" + no;
                    }
                    idIdentitas = "ID" + no;
                }
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();//penanganan masalah
        }
    }

    private void createUser() {
        name = txtName.getText().trim();
        email = txtEmail.getText().trim();
        if (name.isEmpty()) {
            showMessage("Name cannot be empty!");
            txtName.requestFocus();
        } else if (email.isEmpty()) {
            showMessage("Email cannot be empty!");
            txtEmail.requestFocus();
        } else {
            String query = "INSERT INTO `users`(`id_user`, `name`, `email`, `password`,`role`, `is_active`) VALUES (?,?,?,?,?,?)";
            try {
                ps = MyConnection.getConnection().prepareStatement(query);
                ps.setString(1, id);
                ps.setString(2, name);
                ps.setString(3, email);
                ps.setString(4, pass);
                ps.setString(5, role);
                ps.setInt(6, is_active);

                ps.executeUpdate();
                autonumberUser();
//                showMessage("Account registration successful!");
                createIdentitas();
            } catch (Exception e) {
                showMessage("Account registration failed" + e);
            }
        }
    }

    private void createIdentitas() {
        //get id_user
        String id_user = null;
        email = txtEmail.getText().trim();
        String query = "SELECT `id_user` FROM `users` WHERE `email` ='" + email + "'";
        try {
            ps = MyConnection.getConnection().prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                Object[] ob = new Object[2];
                ob[0] = rs.getString(1);
                id_user = (String) ob[0];
            }
            rs.close();
            ps.close();

            name = txtName.getText().trim();
            if (rLaki.isSelected()) {
                jenisKelamin = "Laki-laki";
            } else {
                jenisKelamin = "Perempuan";
            }
            tempatLahir = txtTempatLahir.getText().trim();
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            tanggal = String.valueOf(dateFormat.format(txtTanggalLahir.getDate()));
            handPhone = txtHandphone.getText().trim();
            statusPernikahan = comboStatus.getSelectedItem().toString();
            agama = comboAgama.getSelectedItem().toString();
            noKtp = txtNoKtp.getText().trim();
            alamat = txtAlamatKtp.getText().trim();
            domisili = txtDomisili.getText().trim();

            String sql = "INSERT INTO `identitas`(`id`, `jenisKelamin`, `tempatLahir`, `tanggalLahir`, `handphone`, `status`, `agama`, `noKtp`, `alamatKtp`, `domisili`, `user_id`, `created_by`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
            try {
                ps = MyConnection.getConnection().prepareStatement(sql);
                ps.setString(1, idIdentitas);
                ps.setString(2, jenisKelamin);
                ps.setString(3, tempatLahir);
                ps.setString(4, tanggal);
                ps.setString(5, handPhone);
                ps.setString(6, statusPernikahan);
                ps.setString(7, agama);
                ps.setString(8, noKtp);
                ps.setString(9, alamat);
                ps.setString(10, domisili);
                ps.setString(11, id_user);
                ps.setString(12, created_by);

                ps.executeUpdate();
                showMessage("Successfully!");
                this.dispose();
            } catch (Exception e) {
                showMessage("Error" + e);
            }
        } catch (SQLException ex) {
            showMessage("Error " + ex);
        }
    }

    private void showMessage(String message) {
        JOptionPane.showMessageDialog(null, message);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTextField5 = new javax.swing.JTextField();
        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        txtEmail = new javax.swing.JTextField();
        rLaki = new javax.swing.JRadioButton();
        rPerempuan = new javax.swing.JRadioButton();
        txtTempatLahir = new javax.swing.JTextField();
        txtTanggalLahir = new com.toedter.calendar.JDateChooser();
        txtHandphone = new javax.swing.JTextField();
        comboStatus = new javax.swing.JComboBox<>();
        comboAgama = new javax.swing.JComboBox<>();
        txtNoKtp = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtAlamatKtp = new javax.swing.JTextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtDomisili = new javax.swing.JTextArea();
        btnBatal = new javax.swing.JButton();
        btnUbah = new javax.swing.JButton();
        btnSimpan = new javax.swing.JButton();

        jTextField5.setText("jTextField5");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153, 153, 153), 1, true));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Formulir Identitas");

        jLabel2.setText("Nama");

        jLabel3.setText("Email");

        jLabel4.setText("Jenis Kelamin");

        jLabel5.setText("Tempat Lahir");

        jLabel6.setText("Tanggal Lahir");

        jLabel7.setText("Handphone");

        jLabel8.setText("Status Pernikahan");

        jLabel9.setText("Agama");

        jLabel10.setText("No. KTP");

        jLabel11.setText("Alamat KTP");

        jLabel12.setText("Domisili");

        rLaki.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup1.add(rLaki);
        rLaki.setText("Laki-laki");

        rPerempuan.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup1.add(rPerempuan);
        rPerempuan.setText("Perempuan");

        txtTanggalLahir.setDateFormatString("dd-MM-yyyy");

        comboStatus.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Lajang", "Kawin", "Janda", "Duda" }));

        comboAgama.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Islam", "Kristen", "Katolik", "Hindu", "Budha" }));

        txtAlamatKtp.setColumns(20);
        txtAlamatKtp.setRows(1);
        txtAlamatKtp.setTabSize(1);
        jScrollPane1.setViewportView(txtAlamatKtp);

        txtDomisili.setColumns(20);
        txtDomisili.setRows(1);
        txtDomisili.setTabSize(1);
        jScrollPane2.setViewportView(txtDomisili);

        btnBatal.setBackground(new java.awt.Color(255, 51, 51));
        btnBatal.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnBatal.setForeground(new java.awt.Color(255, 255, 255));
        btnBatal.setText("Batal");
        btnBatal.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBatalActionPerformed(evt);
            }
        });

        btnUbah.setBackground(new java.awt.Color(255, 255, 51));
        btnUbah.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnUbah.setForeground(new java.awt.Color(51, 51, 51));
        btnUbah.setText("Ubah");
        btnUbah.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnUbah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUbahActionPerformed(evt);
            }
        });

        btnSimpan.setBackground(new java.awt.Color(45, 83, 237));
        btnSimpan.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnSimpan.setForeground(new java.awt.Color(255, 255, 255));
        btnSimpan.setText("Simpan");
        btnSimpan.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSimpanActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnSimpan)
                .addGap(10, 10, 10)
                .addComponent(btnUbah)
                .addGap(10, 10, 10)
                .addComponent(btnBatal)
                .addGap(10, 10, 10))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtName)
                            .addComponent(txtEmail)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(rLaki)
                                .addGap(29, 29, 29)
                                .addComponent(rPerempuan, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txtTempatLahir)
                            .addComponent(txtHandphone)
                            .addComponent(comboStatus, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(comboAgama, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtNoKtp)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 302, Short.MAX_VALUE)
                            .addComponent(jScrollPane2)
                            .addComponent(txtTanggalLahir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(rLaki)
                    .addComponent(rPerempuan))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtTempatLahir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(txtTanggalLahir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtHandphone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(comboStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(comboAgama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(txtNoKtp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel11)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel12)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnBatal)
                    .addComponent(btnUbah)
                    .addComponent(btnSimpan))
                .addGap(25, 25, 25))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBatalActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_btnBatalActionPerformed

    private void btnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSimpanActionPerformed
        // TODO add your handling code here:
        createUser();
    }//GEN-LAST:event_btnSimpanActionPerformed

    private void btnUbahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUbahActionPerformed
        // TODO add your handling code here:
        name = txtName.getText().trim();
        email = txtEmail.getText().trim();
        if (rLaki.isSelected()) {
            jenisKelamin = "Laki-laki";
        } else {
            jenisKelamin = "Perempuan";
        }
        tempatLahir = txtTempatLahir.getText().trim();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        tanggal = String.valueOf(dateFormat.format(txtTanggalLahir.getDate()));
        handPhone = txtHandphone.getText().trim();
        statusPernikahan = comboStatus.getSelectedItem().toString();
        agama = comboAgama.getSelectedItem().toString();
        noKtp = txtNoKtp.getText().trim();
        alamat = txtAlamatKtp.getText().trim();
        domisili = txtDomisili.getText().trim();

        String query = "UPDATE `identitas` SET `jenisKelamin`=?,`tempatLahir`=?,`tanggalLahir`=?,`handphone`=?,`status`=?,`agama`=?,`noKtp`=?,`alamatKtp`=?,`domisili`=? WHERE `id`='" + id_user + "'";
        try {
            ps = MyConnection.getConnection().prepareStatement(query);
            ps.setString(1, jenisKelamin);
            ps.setString(2, tempatLahir);
            ps.setString(3, tanggal);
            ps.setString(4, handPhone);
            ps.setString(5, statusPernikahan);
            ps.setString(6, agama);
            ps.setString(7, noKtp);
            ps.setString(8, alamat);
            ps.setString(9, domisili);
            ps.executeUpdate();
            showMessage("Data successfully changed!");
            this.dispose();

        } catch (Exception e) {
            showMessage("Data failed to change! " + e);
        }

    }//GEN-LAST:event_btnUbahActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(IdentitasForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(IdentitasForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(IdentitasForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(IdentitasForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new IdentitasForm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBatal;
    public javax.swing.JButton btnSimpan;
    public javax.swing.JButton btnUbah;
    private javax.swing.ButtonGroup buttonGroup1;
    public javax.swing.JComboBox<String> comboAgama;
    public javax.swing.JComboBox<String> comboStatus;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField jTextField5;
    public javax.swing.JRadioButton rLaki;
    public javax.swing.JRadioButton rPerempuan;
    public javax.swing.JTextArea txtAlamatKtp;
    public javax.swing.JTextArea txtDomisili;
    public javax.swing.JTextField txtEmail;
    public javax.swing.JTextField txtHandphone;
    public javax.swing.JTextField txtName;
    public javax.swing.JTextField txtNoKtp;
    public com.toedter.calendar.JDateChooser txtTanggalLahir;
    public javax.swing.JTextField txtTempatLahir;
    // End of variables declaration//GEN-END:variables
}
