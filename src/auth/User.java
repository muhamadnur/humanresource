/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auth;

/**
 *
 * @author Muhamad Nur Sukur
 */
public class User {
    private static String id_user;
    private static String name;
    private static String role;
    private static String email;
    private static int is_active;

    public User() {
    }

    public static String getEmail() {
        return email;
    }

    public static void setEmail(String email) {
        User.email = email;
    }

    public static String getId_user() {
        return id_user;
    }

    public static void setId_user(String id_user) {
        User.id_user = id_user;
    }

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        User.name = name;
    }

    public static String getRole() {
        return role;
    }

    public static void setRole(String role) {
        User.role = role;
    }
    

    public static int getIs_active() {
        return is_active;
    }

    public static void setIs_active(int is_active) {
        User.is_active = is_active;
    }
    
}
